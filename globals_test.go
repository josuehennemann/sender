package main

import (
	"testing"
)

func TestFindReverseIP(t *testing.T) {

	table := []struct {
		Ip      string // IPv4
		Reverse string // nome do reverso
		Failed  bool   // Flag que indica se esse IP vai dar erro ou nao
	}{
		{"192.168.0.1", "", true},
		{"200.146.227.169", "200-146-227-169.xf-static.ctbcnetsuper.com.br", false},
		{"172.217.29.163", "gru10s02-in-f163.1e100.net", false},
	}

	for _, v := range table {
		name, e := findReverseIP(v.Ip, 3)
		if v.Failed && e == nil {
			t.Error("Failed find reverse, expected error to find reverse")
			continue
		}
		if name != v.Reverse {
			t.Error("Failed find reverse, expected", v.Reverse, "find", name)
		}

	}
}
