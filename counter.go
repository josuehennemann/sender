package main

import (
	"encoding/json"
	"github.com/josuehennemann/logger"
	"io/ioutil"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

const (
	COUNTER_DIR = "counter_request/"
)

type Counter struct {
	Count uint64
	name  string
	file  string
	sync.Mutex
}

func NewCounter(name string) *Counter {
	c := &Counter{}
	c.file = config.DataPath + COUNTER_DIR + name + "_data.json"
	c.name = name
	tmp, err := ioutil.ReadFile(c.file)

	txtLog := "Arquivo de counter [%s] não encontrado, iniciando sem o contador"
	if len(tmp) > 0 && err == nil {
		txtLog = "Carregou arquivo de counter [%s]"

		if err := json.Unmarshal(tmp, &c); err != nil {
			Logger.Printf(logger.ERROR, "Falha ao tentar fazer unmarshal do counter [%s]. Erro[%s][%s]", name, err, string(tmp))
			return nil
		}
	}
	go c.autoSave()
	Logger.Printf(logger.INFO, txtLog, name)
	return c
}

//incrementa e retorna o countador
func (c *Counter) GetNextValue() uint64 {
	//faz o lock só por prevenção devido a função autosave
	c.Lock()
	defer c.Unlock()

	atomic.AddUint64(&c.Count, 1)
	return atomic.LoadUint64(&c.Count)
}

//mesma coisa da função GetNextValue, porém retorna em string
func (c *Counter) GetNextValueString() string {
	return strconv.FormatUint(c.GetNextValue(), 10)
}

//retorna apenas o ultimo valor gerado
func (c *Counter) GetLastValue() uint64 {
	return atomic.LoadUint64(&c.Count)
}

//mesma coisa da função GetLastValue, porém retorna em string
func (c *Counter) GetLastValueString() string {
	return strconv.FormatUint(c.GetLastValue(), 10)
}

// função que persiste o counter em disco
func (c *Counter) Save() {
	c._save()

	Logger.Printf(logger.INFO, "Arquivo do counter [%s] de mx salvo com sucesso", c.name)
}

//função interna que a cada 30s salva o contador em disco
func (c *Counter) autoSave() {
	defer recoverPanic()
	for {
		//se entrou em desligamento, nao precisa salvar, pois o proceso de desligamento faz isso
		if turnoff.IsShutdown() {
			return
		}
		time.Sleep(time.Second * 30)
		func() {
			c.Lock()
			defer c.Unlock()
			c._save()
		}()
	}
}
func (c *Counter) _save() {
	data, err := json.Marshal(c)
	if err != nil {
		Logger.Printf(logger.ERROR, "Falha ao tentar fazer marshal do counter [%s] de mx. Erro[%s]", c.name, err)
		return
	}

	err = ioutil.WriteFile(c.file, data, 0666)
	if err != nil {
		Logger.Printf(logger.ERROR, "Falha ao tentar salvar arquivo do counter [%s] de mx. Erro[%s][%s]", c.name, err, string(data))
		return
	}
}
