package main

import (
	"encoding/json"
	"fmt"
	"github.com/josuehennemann/diskqueue"
	"github.com/josuehennemann/logger"
	"time"
)

type ControlQueue struct {
	contact           *diskqueue.DiskQueue // fila principal de entrada de contatos
	contactNow        *diskqueue.DiskQueue // fila de entrada de contatos que são enviados na hora (não entram na fila normal)
	retriesN          map[uint8]*diskqueue.DiskQueue
	bounce            *diskqueue.DiskQueue // fila com os erros
	returnPath        *diskqueue.DiskQueue // fila com os emails de bounce recebidos
	channelContact    chan *SendData
	channelContactNow chan *SendData
	numProc           uint8
}

func NewControlQueue() (*ControlQueue, error) {
	var err error
	cq := &ControlQueue{}
	cq.retriesN = map[uint8]*diskqueue.DiskQueue{}
	cq.contact, err = diskqueue.New(config.DataPath + "queues/contact")
	if err != nil {
		return nil, err
	}

	cq.contactNow, err = diskqueue.New(config.DataPath + "queues/contactNow")
	if err != nil {
		return nil, err
	}

	var MaxRetry = uint8(10)
	for i := uint8(1); i <= MaxRetry; i++ {
		cq.retriesN[i], err = diskqueue.New(config.DataPath + "queues/retries_" + fmt.Sprintf("%d", i))
		if err != nil {
			return nil, err
		}
	}

	cq.bounce, err = diskqueue.New(config.DataPath + "queues/bounce")
	if err != nil {
		return nil, err
	}

	cq.returnPath, err = diskqueue.New(config.DataPath + "queues/returnPath")
	if err != nil {
		return nil, err
	}
	go cq.readQueueReturnPath()

	go cq.readQueueBounce()

	cq.numProc = 15

	cq.channelContact = make(chan *SendData, cq.numProc)
	cq.channelContactNow = make(chan *SendData, cq.numProc)
	for i := uint8(0); i < cq.numProc; i++ {
		go cq.processContact()
		go cq.processContactNow()
	}

	go cq.readQueueContact()
	go cq.readQueueContactNow()

	//retentativas
	for i := uint8(1); i <= MaxRetry; i++ {
		go cq.readQueueNRetryContact(cq.retriesN[i])
	}

	return cq, nil
}

func (cq *ControlQueue) AddContact(data *SendData) error {

	dataPush, err := json.Marshal(data)
	if err != nil {
		return err
	}
	if data.IsReinsert() {
		qN := data.MaxRetry
		//ultima fila de tentativa
		if qN > 10 {
			qN = 10
		}
		cq.retriesN[qN].Push(dataPush)
		Logger.Printf(logger.INFO, "Adicionou o contato [%s] na fila [%d] de retentativas", data.GetContactEmail(), data.MaxRetry)
	} else {
		Logger.Printf(logger.INFO, "Adicionou o contato [%s] na fila de envio", data.GetContactEmail())
		cq.contact.Push(dataPush)
	}
	return nil
}

func (cq *ControlQueue) AddBounce(data *BounceData) error {
	dataPush, err := json.Marshal(data)
	if err != nil {
		return err
	}

	cq.bounce.Push(dataPush)
	return nil
}

//função que le a fila e sabe mandar para as goroutines que fazem o envio
func (cq *ControlQueue) readQueueContact() {
	defer recoverPanic()
	for {
		if turnoff.IsShutdown() {
			Logger.Printf(logger.INFO, "Sistema em desligamento, parando de ler a fila de contatos")
			return
		}

		slcBytes, numRec := cq.contact.PopReadOnly(100)
		if numRec == 0 {
			time.Sleep(time.Second)
			continue
		}

		for _, v := range slcBytes {
			if cq.parseContact(v, cq.channelContact) {
				cq.contact.Pop()
			}
		}
	}
}

func (cq *ControlQueue) parseContact(slcBytes []byte, channel chan *SendData) (makePop bool) {
	//se tentou ler a fila e não tem registro então dorme um segundo
	if len(slcBytes) == 0 {
		time.Sleep(time.Second)
		return
	}

	makePop = true

	//faz o parse dos dados da fila
	tmp := &SendData{}
	if err := json.Unmarshal(slcBytes, tmp); err != nil {
		Logger.Printf(logger.ERROR, "Falha ao tentar fazer unmarshal do email. Erro[%s][%s]", err, string(slcBytes))
		return
	}
	//valida se é um contato que foi reinserido, em caso positivo dorme um pouco
	if tmp.IsReinsert() {
		//se chegou em 10 tentativas entao abandona e marca bounce
		if tmp.MaxRetry == 10 {
			tmp.CaptureBounce(errorMaxRetry)
			return
		}
		time.Sleep(time.Second * (5 * time.Duration(tmp.MaxRetry)))
	}
	//adiciona 1 na wg de envios
	turnoff.AddSend()
	//manda para o channel que processa os envios
	channel <- tmp
	return
}

//função que recebe os dados do channel e realiza o envio
func (cq *ControlQueue) processContact() {

	for data := range cq.channelContact {
		//apenas um tratamente de prevensao, para nao dar panic em caso do channel receber um ponteiro nil
		if data == nil {
			continue
		}
		func() {

			defer turnoff.DoneSend() //garante que sempre vai remover do wg
			data.SendEmail()
		}()
	}
}

//função que le a fila de bounce
//por enquanto nao faz nada com essa informação, da o pop apenas para ir rotando a
func (cq *ControlQueue) readQueueBounce() {
	defer recoverPanic()
	for {
		if turnoff.IsShutdown() {
			Logger.Printf(logger.INFO, "Sistema em desligamento, parando de ler a fila de bounces")
			return
		}
		slcBytes, numRec := cq.bounce.PopReadOnly(1)
		if numRec == 0 {
			time.Sleep(time.Second)
			continue
		}
		func() {
			turnoff.AddBounce()
			defer turnoff.DoneBounce()
			if cq.parseBounce(slcBytes[0]) {
				cq.bounce.Pop()
			}
		}()

	}
}

func (cq *ControlQueue) parseBounce(slcBytes []byte) (makePop bool) {

	//se tentou ler a fila e não tem registro então dorme um segundo
	if len(slcBytes) == 0 {
		time.Sleep(time.Second)
		return
	}

	makePop = true

	//faz o parse dos dados da fila
	tmp := &BounceData{}
	if err := json.Unmarshal(slcBytes, tmp); err != nil {
		Logger.Printf(logger.ERROR, "Falha ao tentar fazer unmarshal do email. Erro[%s][%s]", err, string(slcBytes))
		return
	}

	makePop = tmp.SendRequest()
	return
}

func (cq *ControlQueue) AddMailReturn(data *MailQueue) error {
	dataPush, err := json.Marshal(data)
	if err != nil {
		return err
	}

	cq.returnPath.Push(dataPush)
	return nil
}

//le a fila com os e-mail recebidos de bounce e faz a analise dos mesmos
func (cq *ControlQueue) readQueueReturnPath() {
	defer recoverPanic()
	for {
		if turnoff.IsShutdown() {
			Logger.Printf(logger.INFO, "Sistema em desligamento, parando de ler a fila de e-mail de return")
			return
		}

		slcBytes, numRec := cq.returnPath.PopReadOnly(1)
		if numRec == 0 {
			time.Sleep(time.Second)
			continue
		}

		if cq.parseReturnPath(slcBytes[0]) {
			cq.returnPath.Pop()
		}

	}
}

func (cq *ControlQueue) parseReturnPath(slcBytes []byte) (makePop bool) {
	//se tentou ler a fila e não tem registro então dorme um segundo
	if len(slcBytes) == 0 {
		time.Sleep(time.Second)
		return
	}

	makePop = true

	//faz o parse dos dados da fila
	mr := &MailQueue{}
	if err := json.Unmarshal(slcBytes, mr); err != nil {
		Logger.Printf(logger.ERROR, "Falha ao tentar fazer unmarshal do email de returo. Erro[%s][%s]", err, string(slcBytes))
		return
	}

	//adiciona 1 na wg de envios
	turnoff.AddMailReturn()
	defer turnoff.DoneMailReturn()
	mr.parseEmail()

	return
}

//função que le a fila de retentativas e sabe mandar para as goroutines que fazem o envio
func (cq *ControlQueue) readQueueNRetryContact(q *diskqueue.DiskQueue) {
	defer recoverPanic()
	for {
		if turnoff.IsShutdown() {
			Logger.Printf(logger.INFO, "Sistema em desligamento, parando de ler a fila de retentativas")
			return
		}
		//time.Sleep(time.Second * 10)
		slcBytes, numRec := q.PopReadOnly(1)
		if numRec == 0 {
			time.Sleep(time.Second)
			continue
		}

		if cq.parseContact(slcBytes[0], cq.channelContact) {
			q.Pop()
		}

	}
}

func (cq *ControlQueue) AddContactNow(data *SendData) error {

	dataPush, err := json.Marshal(data)
	if err != nil {
		return err
	}
	if data.IsReinsert() {
		qN := data.MaxRetry
		//ultima fila de tentativa
		if qN > 10 {
			qN = 10
		}
		cq.retriesN[qN].Push(dataPush)
		Logger.Printf(logger.INFO, "Adicionou o contato [%s] na fila [%d] de retentativas", data.GetContactEmail(), data.MaxRetry)
	} else {
		Logger.Printf(logger.INFO, "Adicionou o contato [%s] na fila de envio imediato", data.GetContactEmail())
		cq.contactNow.Push(dataPush)
	}
	return nil
}

//função que recebe os dados do channel e realiza o envio
func (cq *ControlQueue) processContactNow() {

	for data := range cq.channelContactNow {
		//apenas um tratamente de prevensao, para nao dar panic em caso do channel receber um ponteiro nil
		if data == nil {
			continue
		}
		func() {

			defer turnoff.DoneSend() //garante que sempre vai remover do wg
			data.SendEmail()
		}()
	}
}



//função que le a fila e sabe mandar para as goroutines que fazem o envio
func (cq *ControlQueue) readQueueContactNow() {
	defer recoverPanic()
	for {
		if turnoff.IsShutdown() {
			Logger.Printf(logger.INFO, "Sistema em desligamento, parando de ler a fila de contatos imediato")
			return
		}

		slcBytes, numRec := cq.contactNow.PopReadOnly(100)
		if numRec == 0 {
			time.Sleep(time.Second)
			continue
		}

		for _, v := range slcBytes {
			if cq.parseContact(v,cq.channelContactNow) {
				cq.contactNow.Pop()
			}
		}
	}
}
