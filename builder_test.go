package main

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
	"testing"
)

func BenchmarkRBWH(b *testing.B) {
	for n := 0; n < b.N; n++ {
		randomBoundaryWithHex()
	}
}

func BenchmarkRBWF(b *testing.B) {
	for n := 0; n < b.N; n++ {
		randomBoundaryWithFmt()
	}
}

func randomBoundaryWithHex() string {
	buf := make([]byte, 15)
	_, err := io.ReadFull(rand.Reader, buf)
	if err != nil {
		panic(err)
	}
	return hex.EncodeToString(buf)
}

func randomBoundaryWithFmt() string {
	buf := make([]byte, 15)
	_, err := io.ReadFull(rand.Reader, buf)
	if err != nil {
		panic(err)
	}
	return fmt.Sprintf("%x", buf)
}

/*
result:
$ go test -bench=. -run=Ben
goos: windows
goarch: amd64
BenchmarkRBWH-4          3000000               398 ns/op
BenchmarkRBWF-4          3000000               523 ns/op
*/
