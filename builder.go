package main

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"encoding/pem"
	"io"
	"mime/multipart"
	"net/textproto"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

const (
	CRLF          = "\r\n"
	MIME_VERSION  = "1.0"
	PROLOGUE      = "This is a multi-part message in MIME format."
	CHARSET       = "UTF-8"
	FORMATDATE    = "Mon, 02 Jan 2006 15:04:05 -0700 (MST)" // RFC2821
	MAX_CHAR_LINE = 74
	REGEXP_TAGS   = "{{[a-zA-Z0-9_]*}}"

	HEADER_FROM            = "From"
	HEADER_TO              = "To"
	HEADER_SUBJECT         = "Subject"
	HEADER_LISTUNSUBSCRIBE = "List-Unsubscribe"
	HEADER_DKIM            = "h=" + HEADER_FROM + ":" + HEADER_TO + ":" + HEADER_SUBJECT + ";" + CRLF
	DKIM_SIGN              = "dkim-signature"
)

var (
	reCRTags                   = regexp.MustCompile(REGEXP_TAGS)
	reFindListUnsubscribeValue = regexp.MustCompile("^<(.*)>$")
	reFindEmailValues          = regexp.MustCompile("<([^<]*)>$")
	reReplaceCRLFAndTab        = regexp.MustCompile(CRLF + "\t+")
	reReplaceSpace             = regexp.MustCompile("[ ]+")
)

type Mail struct {
	header     textproto.MIMEHeader
	rawText    string
	text       string
	rawHtml    string
	html       string
	fullMail   *bytes.Buffer
	tagsHeader map[string][]string
	DKIM       *DKIM
	boundary   string
}

func NewEmail() *Mail {
	m := &Mail{}
	m.tagsHeader = make(map[string][]string, 16)

	m.header = make(textproto.MIMEHeader)
	m.AddHeader("mime-version", MIME_VERSION)

	m.fullMail = new(bytes.Buffer)

	m.DKIM = newDKIM()

	return m
}

func (m *Mail) SetHtml(template string) {
	m.rawHtml = template
}

func (m *Mail) SetText(template string) {
	m.rawText = template
}

func (m *Mail) AddHeader(key, value string) {
	r := findTagStringRegexp(value)
	if len(r) > 0 {
		m.tagsHeader[textproto.CanonicalMIMEHeaderKey(key)] = r
	}

	m.header.Set(key, value)
}

func (m *Mail) DelHeader(key string) {
	m.header.Del(key)
}

func (m *Mail) SetBoundary(b string) {
	rbound := randomBoundary()
	//garante que o tamanho do boundary está entre 1 e 69
	if len(b)+len(rbound) >= 1 && len(b)+len(rbound) <= 69 {
		m.boundary = b
	}
}

func (m *Mail) SetFixedBoundary(b string) {
	n := len(b)
	if n > 69 {
		n = 69
	}
	m.boundary = b[:n]
}

func (m *Mail) writeBody(b *bytes.Buffer) {
	var part io.Writer

	if m.boundary == "" {
		m.boundary = randomBoundary()
	}

	alternative := multipart.NewWriter(b)
	// Se tiver boundary configurado, concatena com o random
	// para o alternative
	alternative.SetBoundary(m.boundary)

	boundary := alternative.Boundary()

	m.AddHeader("content-type", "multipart/alternative;"+CRLF+"\tboundary=\""+boundary+"\"")

	b.Write([]byte(CRLF))
	b.Write([]byte(PROLOGUE))
	b.Write([]byte(CRLF))

	if len(m.text) > 0 {
		// Inicia parte texto
		ht := make(textproto.MIMEHeader)
		ht.Set("content-transfer-encoding", "quoted-printable")
		ht.Set("content-type", "text/plain;"+CRLF+"\tcharset=\""+CHARSET+"\"")
		part, _ = alternative.CreatePart(ht)

		part.Write([]byte(m.text))
		b.Write([]byte(CRLF))
	}

	if len(m.html) > 0 {
		// Inicia parte HTML
		hh := make(textproto.MIMEHeader)
		hh.Set("content-transfer-encoding", "quoted-printable")
		hh.Set("content-type", "text/html;"+CRLF+"\tcharset=\""+CHARSET+"\"")
		part, _ = alternative.CreatePart(hh)

		part.Write([]byte(m.html))
		b.Write([]byte(CRLF))
	}
	alternative.Close()
}

func (m *Mail) setDKIM(body string) {

	m.DelHeader(DKIM_SIGN)

	if !m.DKIM.using {
		return
	}

	// Copia conteudo do header para as variaveis
	fromHeader := HEADER_FROM + ":" + getPrintableHeader(HEADER_FROM, m.header.Get("from"), nil, nil)
	toHeader := HEADER_TO + ":" + getPrintableHeader(HEADER_TO, m.header.Get("to"), nil, nil)
	subjectHeader := HEADER_SUBJECT + ":" + getPrintableHeader(HEADER_SUBJECT, m.header.Get("subject"), nil, nil)

	// Prepara campos para inserir na assinatura
	from := strings.Replace(quotedPrintableDKIM(fromHeader), "|", "=7C", -1)
	to := strings.Replace(quotedPrintableDKIM(toHeader), "|", "=7C", -1)
	subject := strings.Replace(quotedPrintableDKIM(subjectHeader), "|", "=7C", -1)
	body = m.DKIM.cleanBody(body)

	// Tamanho do corpo da mensagem
	DKIM_l := int64(len(body))

	// Criptografando corpo com base64 do resultado de SHA1
	sha1 := sha1.New()
	//io.WriteString(sha1, body)
	sha1.Write([]byte(body))
	DKIM_bh := base64.StdEncoding.EncodeToString(sha1.Sum(nil))

	iPart := ""
	if m.DKIM.subdomain != "" {
		iPart = " i=" + m.DKIM.subdomain + ";"
	}

	//Monta os dados da assinatura do dkim
	dkimSign := "v=1; "
	dkimSign += "a=" + m.DKIM.alghoritm + "; "
	dkimSign += "q=" + m.DKIM.method + "; "
	dkimSign += "l=" + strconv.FormatInt(DKIM_l, 10) + "; "
	dkimSign += "s=" + m.DKIM.selector + ";" + CRLF
	dkimSign += "\t" + "t=" + strconv.FormatInt(m.DKIM.date.Unix(), 10) + "; c=" + m.DKIM.canonical + ";" + CRLF
	dkimSign += "\t" + HEADER_DKIM
	dkimSign += "\t" + "d=" + m.DKIM.domain + ";" + iPart + CRLF
	dkimSign += "\t" + "z=" + from + CRLF
	dkimSign += "\t" + "|" + to + CRLF
	dkimSign += "\t" + "|" + subject + ";" + CRLF
	dkimSign += "\t" + "bh=" + DKIM_bh + ";" + CRLF
	dkimSign += "\t" + "b="

	// Chave criptografada final
	stringSign := m.DKIM.prepareSignature(fromHeader + CRLF + toHeader + CRLF + subjectHeader + CRLF + DKIM_SIGN + ":" + dkimSign)
	b := m.DKIM.generateSign(stringSign)

	if b == "" {
		return
	}

	m.AddHeader(DKIM_SIGN, dkimSign+b)
}

func (m *Mail) Build(data map[string]string) string {

	// Encontra tags no texto
	tagsText := findTagStringRegexp(m.rawText)

	// Encontra tags no HTML
	tagsHtml := findTagStringRegexp(m.rawHtml)

	// Faz o encode quotedPrintable Text
	m.text = quotedPrintableTextBuffer(findAndReplaceTag(m.rawText, tagsText, data, 0), MAX_CHAR_LINE)
	// Faz o encode quotedPrintable HTML
	m.html = quotedPrintableEncodeBuffer(findAndReplaceTag(m.rawHtml, tagsHtml, data, 0), MAX_CHAR_LINE)

	var body bytes.Buffer
	m.writeBody(&body)

	s := body.String()

	m.setDKIM(s)

	// Inicia o e-mail pelo header
	printHeader(m.header, m.tagsHeader, data, m.fullMail)

	// Corpo do e-mail
	m.fullMail.WriteString(s)

	return m.fullMail.String()
}

type DKIM struct {
	selector   string
	privateKey rsa.PrivateKey
	alghoritm  string
	canonical  string
	method     string
	domain     string
	subdomain  string
	date       time.Time
	using      bool
}

func newDKIM() *DKIM {
	var dkim DKIM

	dkim.alghoritm = "rsa-sha1"       // Algoritmo hash que será usado na assinatura
	dkim.canonical = "relaxed/simple" // Canonicação utilizada header/body
	dkim.method = "dns/txt"           // Query method
	dkim.subdomain = ""

	return &dkim
}

func (dkim *DKIM) SetSelector(str string) {
	dkim.selector = str
}

func (dkim *DKIM) SetDomain(str string) {
	dkim.domain = str
}

func (dkim *DKIM) SetPrivateKey(str string) {
	key, errdecode := pem.Decode([]byte(str))
	if len(errdecode) != 0 {
		dkim.using = false
		return
	}
	priv, err := x509.ParsePKCS1PrivateKey(key.Bytes)
	if err == nil {
		dkim.privateKey = *priv
		dkim.using = true
		return
	}

	dkim.using = false
}

func (dkim *DKIM) SetDate(t time.Time) {
	dkim.date = t
}

func (dkim *DKIM) prepareSignature(str string) string {
	// Remove quebras de linhas com espaços
	str = reReplaceCRLFAndTab.ReplaceAllString(str, " ")

	// Explode headers e dá um lowercase
	lines := strings.Split(str, CRLF)
	for key, line := range lines {
		info := strings.SplitN(line, ":", 2)
		heading := strings.ToLower(info[0])
		// Remove espaços duplicados
		value := reReplaceSpace.ReplaceAllString(info[1], " ")
		// Remove espaços desnecessarios
		lines[key] = heading + ":" + strings.TrimSpace(value)
	}

	// Transforma em uma string novamente
	str = strings.Join(lines, CRLF)
	return str
}

//remove todas as possiveis quebras de linha e deixa somente uma no final
func (dkim *DKIM) cleanBody(body string) string {
	body = strings.Trim(body, CRLF)
	body += CRLF

	return body
}

//Gerar uma assinatura de dkim, utilizando a chave privada informada
func (dkim *DKIM) generateSign(str string) string {
	h := sha1.New()
	h.Write([]byte(str))
	digest := h.Sum(nil)

	signature, err := rsa.SignPKCS1v15(nil, &dkim.privateKey, crypto.SHA1, digest)
	if err == nil {
		return base64.StdEncoding.EncodeToString([]byte(signature))
	}

	return ""
}

func findTagStringRegexp(s string) []string {
	var r []string

	matchesS := reCRTags.FindAllString(s, -1)
	mapControl := map[string]struct{}{}
	exists := false
	for _, v := range matchesS {
		value := v[2 : len(v)-2]
		if _, exists = mapControl[value]; !exists {
			r = append(r, value)
		}
	}
	return r
}

func quotedPrintableEncode(data string, length int) string {
	var r, aL string
	var k, tl int

	// padroniza \n
	data = strings.Replace(data, "\r\n", "\n", -1)
	lines := strings.Split(data, "\n")

	// Procura por tags personalizadas
	for _, l := range lines {
		l = reCRTags.ReplaceAllStringFunc(l, func(s string) string { return s })
		ls := strings.Split(l, "\n")

		tl = len(ls)
		aL = ""
		if tl > 1 {
			aL = "="
		}

		for k, l = range ls {
			l = quotedPrintableLineBuffer(l, length)
			if tl == k+1 {
				aL = ""
			}
			r += l + aL + CRLF
		}
	}

	return strings.Trim(r, CRLF)
}

func charToHex(s string, c rune) string {
	strRet := ""

	v := strings.ToUpper(hex.EncodeToString([]byte(string(c))))
	for i := 0; i < len(v); i += 2 {
		strRet += s + v[i:i+2]
	}

	return strRet
}

func printHeader(h textproto.MIMEHeader, headerTags map[string][]string, tagFields map[string]string, b *bytes.Buffer) {
	// organizar o header para gerar templates iguais
	items := make([]string, len(h))
	id := 0
	for k := range h {
		items[id] = k
		id++
	}

	sort.Strings(items)

	for _, k := range items {
		vv := h[k]
		for _, v := range vv {
			v = getPrintableHeader(k, v, headerTags[k], tagFields)
			b.WriteString(k + ": " + v + CRLF)
		}
	}
}

func getPrintableHeader(key, value string, tags []string, tagFields map[string]string) string {
	if len(tags) > 0 {
		value = findAndReplaceTag(value, tags, tagFields, 0)
	}

	if key == HEADER_SUBJECT {
		value = quotedPrintableHeaderBuffer(value, MAX_CHAR_LINE)
	}

	if key == HEADER_LISTUNSUBSCRIBE {
		matches := reFindListUnsubscribeValue.FindStringSubmatch(value)
		if len(matches) < 2 {
			value = "<" + value + ">"
		}
	}

	if key == HEADER_TO || key == HEADER_FROM {
		var (
			email = "<" + value + ">"
			nome  string
		)

		matches := reFindEmailValues.FindStringSubmatch(value)
		if len(matches) == 2 {
			email = "<" + matches[1] + ">"
			nome = strings.Replace(value, email, "", -1)
			nome = strings.TrimSpace(nome)
			nome = strings.Trim(nome, "\"'")
			if nome != "" {
				nome = quotedPrintableHeaderBuffer(nome, 78) + " "
			}
		}

		value = nome + email
	}

	return value
}

func findAndReplaceTag(str string, tags []string, tagFields map[string]string, pos int) string {
	if pos >= len(tags) {
		return str
	}
	return findAndReplaceTag(strings.Replace(str, "{{"+tags[pos]+"}}", tagFields[tags[pos]], -1), tags, tagFields, pos+1)
}

func quotedPrintableLineBuffer(line string, length int) string {
	var (
		content     = new(bytes.Buffer)
		currentLine = new(bytes.Buffer)
	)

	for n, c := range line {
		char := string(c)
		// Se for igual a espaço, concatena =20
		if c == ' ' && n == (len(line)-1) {
			char = "=20"
		} else if c == '\t' {
			// Mantem o tab
		} else if c == '=' || c < ' ' || c > '~' {
			char = charToHex("=", c)
		}

		if (currentLine.Len() + len(char)) > length {
			content.WriteString(currentLine.String() + "=" + CRLF)
			currentLine.Reset()
		}

		currentLine.WriteString(char)
	}
	content.WriteString(currentLine.String())

	return content.String()
}

func quotedPrintableHeaderBuffer(str string, length int) string {
	var (
		content       = new(bytes.Buffer)
		currentBuffer = new(bytes.Buffer)
	)
	escape := "="
	escapeInit := "=?" + CHARSET + "?Q?"
	escapeFinish := "?="
	currentBuffer.WriteString(escapeInit)

	for _, c := range str {
		char := string(c)
		if c == 32 {
			char = "_"
		} else if c == 9 {
			// Mantem o tab
		} else if c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == '}' || c == 61 || c == 60 ||
			c == 62 || c == 44 || c == 58 || c == 63 || c == 64 || c == '_' || c < 32 || c > 126 {
			// "=", "<", ">", ",", ":", "?", "@" e caracteres menores que 32 e caracteres maiores que 126
			char = charToHex(escape, c)
		}

		if (currentBuffer.Len() + len(char) + len(escapeFinish)) > length {
			content.WriteString(currentBuffer.String() + escapeFinish + CRLF)
			currentBuffer.Reset()
			currentBuffer.WriteString("\t" + escapeInit)
		}

		currentBuffer.WriteString(char)
	}
	content.WriteString(currentBuffer.String() + escapeFinish)

	return content.String()
}

func quotedPrintableTextBuffer(data string, length int) string {
	return strings.Trim(quotedPrintableLineBuffer(data, length), CRLF)
}

func quotedPrintableEncodeBuffer(data string, length int) string {
	var r, aL string
	var k, tl int

	// padroniza \n
	data = strings.Replace(data, "\r\n", "\n", -1)
	lines := strings.Split(data, "\n")

	for _, l := range lines {
		l = reCRTags.ReplaceAllStringFunc(l, func(s string) string { return s })
		ls := strings.Split(l, "\n")

		tl = len(ls)
		aL = ""
		if tl > 1 {
			aL = "="
		}

		for k, l = range ls {
			l = quotedPrintableLineBuffer(l, length)
			if tl == k+1 {
				aL = ""
			}
			r += l + aL + CRLF
		}
	}

	return strings.Trim(r, CRLF)
}

func quotedPrintableDKIMBuffer(txt string) string {
	var line = new(bytes.Buffer)

	for _, c := range txt {
		if (33 <= c && c <= 58) || c == 60 || (62 <= c && c <= 126) {
			line.WriteString(string(c))
		} else {
			line.WriteString(charToHex("=", c))
		}
	}

	return line.String()
}

func quotedPrintableDKIM(txt string) string {
	var char, line string
	escape := "="

	for _, c := range txt {
		char = string(c)
		if (33 <= c && c <= 58) || c == 60 || (62 <= c && c <= 126) {
			line += char
		} else {
			line += charToHex(escape, c)
		}
	}

	return line
}

func randomBoundary() string {
	buf := make([]byte, 15)
	_, err := io.ReadFull(rand.Reader, buf)
	if err != nil {
		panic(err)
	}
	return hex.EncodeToString(buf)
}
