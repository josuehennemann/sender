/*
	Estrutra de cache de MX: O objetivo desse cache é otimizar o processo de envio, para nao realizar várias vezes a mesma operação.
	Montar uma struct que tenha um mapa[string]*struct, para controlar o cache dos endereços de mx.
	essa struct deve conter: endereço do mx, data do ultimo envio e data da ultima e se usa tls
	o map deve ficar nesse formato: [dominio]*struct
	Sempre que precisar consultar um mx, antes devemos verificar se ele esta em cache, e se faz mais de 24h da ultima consulta.
	Criar metodo de purge, para remover uma posição especifica do mapa.
	Criar rotina que remove do mapa items em que faz mais de 30 dias do ultimo envio (isso server para controlar o consumo de memoria)
*/
package main

import (
	"encoding/json"
	"github.com/josuehennemann/logger"
	"io/ioutil"
	"net"
	"sync"
	"time"
)

const (
	CACHEMX_DIR = "cacheMx/"
	TIME_MONTH  = 24 * 30 //24h *30 dias
)

type CacheMX struct {
	list         map[string]*DomainInfo
	sync.RWMutex                 //Tipo de lock que permite multiplas leituras simultaneas mas uma escrita por vez
	file         string          //nome do arquivo onde ficam os dados persistidos
	blacklist    map[string]bool // map com dominios de blacklist para não enviar
}
type DomainInfo struct {
	lastSend time.Time
	mxList   []*MXInfo
	hasError bool //flag que indica se houve erro na resolução de mx
}

type MXInfo struct {
	address net.MX
	ip      []net.IP
	mxError bool // flag que indica que mesmo tendo resolvido tudo, ao tentar enviar e-mail da erro.
}

func NewCacheMx() *CacheMX {
	c := &CacheMX{}
	c.file = config.DataPath + CACHEMX_DIR + "data.json"
	c.list = map[string]*DomainInfo{}
	tmp, err := ioutil.ReadFile(c.file)
	//defer c.gc() //dispara a função de garbage collector, que remove items velhos do cache

	txtLog := "Arquivo de cacheMx não encontrado, iniciando sem cacheMx"
	if len(tmp) > 0 && err == nil {
		txtLog = "Carregou arquivo de cacheMx"

		if err := json.Unmarshal(tmp, &c.list); err != nil {
			Logger.Printf(logger.ERROR, "Falha ao tentar fazer unmarshal do cacheMx. Erro[%s][%s]", err, string(tmp))
			return c
		}
	}

	c.blacklist = map[string]bool{
		"mail.h-email.net": true, // dominio incluido na blacklist, pois é um dominio que só recebe emails de domínios invalidos como: iclound, terrs, gmai, hotmai
	}

	Logger.Println(logger.INFO, txtLog)
	return c
}

/* FUNÇÕES PUBLICAS */

// procura no cache, caso nao encontre, tenta resolver para adicionar
// sempre que encontra o dominio, ele atualiz a data de ultimo envio
func (c *CacheMX) Get(d string) (lmx []net.MX, lip []net.IP) {
	//faz o lock apenas para leitura para verificar se existe no cache
	c.RLock()
	item, ok := c.list[d]
	c.RUnlock()
	//caso nao exista ou faz mais de 24h do ultimo envio, vai resolver o mx do dominio e adicionar
	now := time.Now()
	if !ok || now.Sub(item.lastSend).Hours() >= 24 {
		item = c.add(d)
	}

	//se por acaso nao existe MX para o dominio sai aqui
	if item.hasError {
		return
	}

	for _, data := range item.mxList {
		if data.mxError {
			continue
		}
		lmx = append(lmx, data.address)
		lip = append(lip, data.ip...)
	}
	if len(lmx) > 0 {
		//atualiza a data do ultimo envio
		item.lastSend = time.Now()
	}
	return
}

//remove um dominio do cache
func (c *CacheMX) Delete(d string) {
	c.Lock()
	defer c.Unlock()
	c.del(d)
}

//verifica se o dominio de mx está na blacklist de envio
func (c *CacheMX) CheckBlacklist(s string) bool {
	return c.blacklist[s]
}

//Persiste o cache em disco
func (c *CacheMX) Save() {
	c.Lock()
	defer c.Unlock()
	data, err := json.Marshal(c.list)

	if err != nil {
		Logger.Printf(logger.ERROR, "Falha ao tentar fazer marshal do cacheMx. Erro[%s][%s]", err)
		return
	}

	err = ioutil.WriteFile(c.file, data, 0666)
	if err != nil {
		Logger.Printf(logger.ERROR, "Falha ao tentar salvar do cacheMx. Erro[%s][%s]", err, string(data))
		return
	}

	Logger.Printf(logger.INFO, "Arquivo de cacheMx salvo com sucesso")
}

/* FUNÇÕES PRIVADAS */

//Resolve e adiciona um dominio no cache
func (c *CacheMX) add(d string) (item *DomainInfo) {
	item = &DomainInfo{}
	mxAddress, err := net.LookupMX(d)

	if err != nil {
		Logger.Printf(logger.WARN, "Erro ao tentar resolver mx do dominio [%s][%s]", d, err)
		item.hasError = true
	}
	item.mxList = make([]*MXInfo, len(mxAddress))
	idx := 0
	for _, v := range mxAddress {
		dataMx := *v
		dataIpMX, err := net.LookupIP(dataMx.Host)
		if err != nil {
			Logger.Printf(logger.WARN, "Falha ao tentar descobrir IP do mx [%s][%s]", dataMx.Host, err)
			continue
		}
		//pega somente os endereços ipv4 do mx
		//feito isso pq nem todos provedores tem ipv6, entao para simplificar, vamos trabalhar apenas com ipv4
		finalList := []net.IP{}
		for _, ip := range dataIpMX {
			//valida se é ipv4
			if ip.To4() == nil {
				continue
			}
			finalList = append(finalList, ip)
		}
		if len(finalList) == 0 {
			Logger.Printf(logger.INFO, "MX sem IPv4. Domínio [%s] MX [%s]", d, dataMx.Host)
			continue
		}

		item.mxList[idx] = &MXInfo{}
		item.mxList[idx].address = dataMx
		item.mxList[idx].ip = finalList

		idx++
	}

	c.Lock()
	defer c.Unlock()
	c.list[d] = item

	return
}

func (c *CacheMX) del(d string) {
	delete(c.list, d)
}

//rotina de limpeza, remove dominios que estao a mais de X dias sem participar de um envio
func (c *CacheMX) gc() {
	defer recoverPanic()
	for {
		c._checkGC()
		time.Sleep(time.Hour)
	}
}

func (c *CacheMX) _checkGC() {
	c.Lock()
	defer c.Unlock()
	n := time.Now()
	count := 0
	for domain, item := range c.list {
		if n.Sub(item.lastSend).Hours() >= TIME_MONTH {
			c.del(domain)
			count++
		}
	}
	if count > 0 {
		Logger.Printf(logger.INFO, "Removeu [%d] domínios do cache de mx", count)
	}
}
