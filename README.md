# sender

Sistema para envio de e-mail


#### Dependências:
    https://github.com/josuehennemann/yaml  => pacote responsavel pela leitura do arquivo de conf
    https://github.com/josuehennemann/logger => pacote de log 
    https://github.com/josuehennemann/diskqueue => pacote de fila
    https://github.com/josuehennemann/html2text => pacote que converte um html para texto


#### Executar binário manualmente
./sender -fileConf=conf/sender.dev.yaml

Acessar no server da OVH:
http://vps632926.ovh.net:25011/send-test?email=josuehennemann@gmail.com

####Teste

curl -X POST \
  http://vps632926.ovh.net:25011/sendmail \
  -H 'Authorization: nMIICXAIBAAKBgQC52he' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 73bf6923-b1f5-4db5-a944-573239a804d5' \
  -H 'cache-control: no-cache' \
  -d '{
    "to": "josuehennemann@gmail.com",
    "subject": "Vamos fazer um teste",
    "reply_to": "teste@josue.com",
    "sender_name": "Sprinta",
    "sender_email": "contato@sprinta.com.br",
    "content": {
        "html": "Vem tester o envio maroto",
        "text": "Vem tester o envio maroto"
    },
    "bounce": {
        "url": "http://sendbounce"
    }
}'