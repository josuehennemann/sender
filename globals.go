package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"flag"
	"math/big"
	mrand "math/rand"
	"net"
	"net/smtp"
	"os"
	"runtime/debug"
	"strings"
	"time"

	"github.com/josuehennemann/logger"
)

var (
	fileConf            = flag.String("fileConf", "", "Endereco do arquivo de configuração")
	pidPath             = flag.String("pidPath", "", "Endereço do arquivo com o pid")
	config              *Config
	Logger              *logger.Logger
	Certificate         tls.Certificate //variavel com o certificado
	HasCertificate      bool            //booleano que indica se tem certificado para usar no tls ou nao
	cachemx             *CacheMX
	QueueSend           *ControlQueue
	turnoff             *TurnOffSystem
	ControlLimit        *ControlLimitMX
	MyIPS               []*net.IP
	listDomainsUsingTls = []string{"google.com",
		"hotmail.com",
		"outlook.com",
		"yahoodns.net",
		"mail-tester.com",
	}
	CounterTR    *Counter
	CounterBL    *Counter
	CounterNW    *Counter
	StandardDkim *DkimData
	ControlSend  = NewControlRepeatSend()
)

const (
	SMTP_PORT = "25"
	SLASH     = "/"
	COLON     = ":"
	CONN_TCP  = "tcp"
	CONN_UDP  = "udp"
	BREAKLINE = "\n"
)

//carrega a lista de Ips da máquina para a memoria
func loadIps() ([]*net.IP, error) {
	ifs, err := net.InterfaceAddrs()
	if err != nil {
		return nil, err
	}
	ips := []*net.IP{}
	for _, v := range ifs {
		//retira o barramento do IP, pois ele retorno no seguite formato: 192.168.0.1/XX
		addrs := strings.Split(v.String(), SLASH)[0]
		//descarta endereço Loopback e possiveis IPv6
		if addrs == "127.0.0.1" || strings.Contains(addrs, COLON) {
			continue
		}

		ip := net.ParseIP(addrs)
		ips = append(ips, &ip)

	}
	return ips, nil
}

//funçao que descobre o reverso de um IP
func findReverseIP(ip string, tries int) (name string, err error) {
	var hosts []string
	hosts, err = net.LookupAddr(ip)
	if err != nil || len(hosts) == 0 {
		if tries < 3 {
			tries++
			time.Sleep(2 * time.Second)
			return findReverseIP(ip, tries)
		}
		// Tentou o número de tentativas, então retorna
		return
	}

	name = strings.Trim(hosts[0], ".") //pega sempre a primeira posiçao
	return
}

//Resolve os endereços de MX e monta um slice de TCPAddr na ordem dos que forem respondendo
func createTCPAddrs(listMx []net.IP) (listMxResolve []*net.TCPAddr, mxseq []int, err error) {

	for seq, ip := range listMx {
		var tmp *net.TCPAddr
		tmp, err = net.ResolveTCPAddr(CONN_TCP, joinAddressPort(ip.String(), SMTP_PORT))
		if err != nil {
			//Se deu erro, ou seja, possivelmente é um MX que nao responde, entao pula para o proximo
			continue
		}
		mxseq = append(mxseq, seq)
		listMxResolve = append(listMxResolve, tmp)
	}
	return
}

func resolveIpOut(ipOut string) (*net.TCPAddr, error) {
	return net.ResolveTCPAddr(CONN_TCP, joinAddressPort(ipOut, "0")) //junta com a porta 0 pois o endereço para a funçao ResolveTCPAddr precisa ser no formato [address:port]
}

func joinAddressPort(addr, port string) string {
	// valida se tem que colocar a :porta ou somente porta no address
	if strings.Index(port, ":") != 0 {
		return addr + ":" + port
	}
	return addr + port
}

func newSmtpClient(ipOut, ipMx *net.TCPAddr, reverseIpOut string, timeout time.Duration) (*net.TCPConn, *smtp.Client, error) {
	conn, err := net.DialTCP(CONN_TCP, ipOut, ipMx)
	if err != nil {
		return nil, nil, err
	}
	conn.SetDeadline(time.Now().Add(timeout)) //define o timeout da conexao
	//monta o client de smtp
	client, err := smtp.NewClient(conn, reverseIpOut)
	if err != nil {
		//Caso nao consiga criar um cliente de smtp fecha a conexao
		conn.Close()
	}

	return conn, client, err
}

const (
	CERT_ORGANIZATION_NAME = "Sprinta sports"
	CERT_COUNTRY_CODE      = "BR"
	CERT_PROVINCE          = "Rio Grande do Sul"
	CERT_ORGANIZATION_UNIT = "TI"
	CERT_CITY              = "Porto Alegre"
	CERT_DIR_FILES         = "certificate/"
	CERT_FILE_CRT          = "server.crt"
	CERT_FILE_CA           = "serverCA.key"
)

func loadCertificate() (certificate tls.Certificate, ok bool) {
	var err error
	_, err = os.Open(config.DataPath + CERT_DIR_FILES + CERT_FILE_CRT)
	//Se deu erro para abrir o arquivo do certificado entao gera novos
	txtLog := "Carregando certificados do disco"
	if err != nil {
		txtLog = "Gerando certificados"
		//se nao conseguir gerar os certificados sai fora
		if err = generateCert(); err != nil {
			Logger.Printf(logger.WARN, "Falha ao tentar gerar certificados. Erro [%s]", err)
			return
		}
	}

	//carrega para a memoria os arquivos de certificado
	certificate, err = tls.LoadX509KeyPair(config.DataPath+CERT_DIR_FILES+CERT_FILE_CRT, config.DataPath+CERT_DIR_FILES+CERT_FILE_CA)
	if err != nil {
		Logger.Printf(logger.WARN, "Falha ao tentar carregar certificados. Erro [%s]", err)
		return
	}

	Logger.Printf(logger.INFO, txtLog)

	ok = true
	return
}

func generateCert() error {

	ca := &x509.Certificate{
		SerialNumber: big.NewInt(1653),
		Subject: pkix.Name{
			Organization:       []string{CERT_ORGANIZATION_NAME},
			Country:            []string{CERT_COUNTRY_CODE},
			Province:           []string{CERT_PROVINCE},
			Locality:           []string{CERT_CITY},
			OrganizationalUnit: []string{CERT_ORGANIZATION_UNIT},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(20, 0, 0), // gera um certificado com validade de 20 anos
		IsCA:                  true,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	priv, _ := rsa.GenerateKey(rand.Reader, 4096)
	pub := &priv.PublicKey
	ca_b, err := x509.CreateCertificate(rand.Reader, ca, ca, pub, priv)
	if err != nil {
		return err
	}

	// Public key
	certOut, err := os.Create(config.DataPath + CERT_DIR_FILES + CERT_FILE_CRT)
	if err != nil {
		return err
	}
	defer certOut.Close()
	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: ca_b})

	// Private key
	keyOut, err := os.OpenFile(config.DataPath+CERT_DIR_FILES+CERT_FILE_CA, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}
	defer keyOut.Close()
	pem.Encode(keyOut, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv)})

	return nil
}

func recoverPanic() {
	if rec := recover(); rec != nil {
		stack := debug.Stack()
		Logger.WritePanic(rec, stack)
		return
	}
}

func GetRandonIP() *net.IP {
	mrand.Seed(time.Now().UnixNano())
	r := mrand.Int31n(int32(len(MyIPS)))
	return MyIPS[r]
}

func sendUsingTls(contactMx string) bool {
	if !HasCertificate {
		return false
	}
	for _, v := range listDomainsUsingTls {
		if strings.Contains(contactMx, v) {
			return true
		}
	}

	return false
}
