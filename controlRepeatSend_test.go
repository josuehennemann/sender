package main

import (
	"testing"
	"time"
)

func TestControlRepeatSend(t *testing.T) {
	a := NewControlRepeatSend()

	//contato nao existe, nao bloqueia
	if a.Block("josue@josue.com", "teste") {
		t.Errorf("Falha ao gerar bloqueio, nao deveria bloquear")
	}

	// contato existe e é o mesmo conteudo, entao bloqueio
	if !a.Block("josue@josue.com", "teste") {
		t.Errorf("Falha ao gerar bloqueio deveria bloquear")
	}

	a.list["josue@josue.com"].date = time.Now().AddDate(0, 0, -1)

	if a.Block("josue@josue.com", "teste") {
		t.Errorf("Falha ao gerar bloqueio nao deveria deveria bloquear")
	}

	//contato já existe mas é outro conteudo
	if a.Block("josue@josue.com", "teste 2") {
		t.Errorf("Falha ao gerar bloqueio, nao deveria bloquear")
	}

}
