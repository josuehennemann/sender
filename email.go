package main

import (
	"crypto/tls"
	"fmt"
	"net"
	"net/smtp"
	"regexp"
	"strings"
	"time"

	"github.com/josuehennemann/logger"
)

const (
	REPLYTO            = "reply-to"
	TO                 = "to"
	DATE               = "date"
	FROM               = "from"
	SENDER             = "sender"
	SUBJECT            = "subject"
	SIGNEDBY           = "signed-by"
	MESSAGEID          = "message-id"
	PREFIX_RETURN_PATH = "return"
	RETURNPATH         = "return-path"
	FEEDBACK_ID        = "feedback-id"

	//tipos de envio
	EMAIL_TYPE_TR = "TR" //e-mails transacionais. São e-mails unitários, ou seja um codigo de envio por contato
	EMAIL_TYPE_BL = "BL" //e-mails em lote (bulk), São e-mails enviados em lote, ou seja o mesmo código de envio para vários contatos
	EMAIL_TYPE_NW = "NW" //e-mails transacionais. São e-mails unitários, ou seja um codigo de envio por contato

//	LISTUNSUBSCRIBE = "list-unsubscribe"
)

var (
	regexpEmailGeneric = regexp.MustCompile("^[a-zA-Z0-9_!#$%&'*+/=?^`{|}~-]+(\\.[a-zA-Z0-9_!#$%&'*+/=?^`{|}~-]+)*@(([a-zA-Z0-9-]+\\.)+[A-Za-z]{2,})$")
	regexpHotmail      = regexp.MustCompile("^[a-z]+([\\.]?[a-z0-9_-]+)*$")
	regexpGmail        = regexp.MustCompile("^[a-z0-9]+(\\.[a-z0-9]+)*$")
	regexpYahoo        = regexp.MustCompile("^[a-z]+([\\._]?[a-z0-9]+)*$")

	ListRegexp = map[string]*regexp.Regexp{
		"hotmail.com":    regexpHotmail,
		"outlook.com":    regexpHotmail,
		"outlook.com.br": regexpHotmail,
		"gmail.com":      regexpGmail,
		"yahoo.com":      regexpYahoo,
		"yahoo.com.br":   regexpYahoo,
	}

	stdError = "Falha ao enviar e-mail para o contato [%s], erro [%s]"
)

//Valida se a string é de um e-mail com sintaxe correta.
//Primeiro é aplicado uma validação generica e depois especifica para os principais provedores
//Pois os mesmos possuem regras para a formação de seus e-mail
/*func isEmail(email string) bool {

	if !regexpEmailGeneric.MatchString(email) {
		return false
	}
	//separa usuario e dominio do e-mail
	tmp := strings.Split(email, "@")
	user := tmp[0]
	domain := tmp[1]
	re := ListRegexp[domain]
	if re != nil && !re.MatchString(user) {
		return false
	}

	return true
}
*/
type SendData struct {
	CodeSend      string        // codigo do envio
	MaxRetry      uint8         //quantidade de retentativas, isso também vai servir como multiplicador do intervalo de tempo de tentativas
	Subject       string        //assunto do envio
	ReplyTo       string        // endereço de retorno
	SenderEmail   string        // e-mail do remetente
	SenderName    string        // nome do rementente
	Dkim          *DkimData     //dados para a assinatura do dkim
	Contact       *ContactData  //Dados do contato
	Template      *TemplateData //template
	BounceRequest *InfoBounce   //dados para enviar o bounce
	TypeSend      string
	ipOut         string // utilizado para em retentativas utilizar o mesmo IP
	reverseIpOut  string // utilizado para em retentativas utilizar o mesmo IP (utilizado também no message Id)
	infolog       string //variavel com o texto padrao para gravar no log

	//campo não utilizado
	ClientCode string //Codigo do cliente,  utilizado para saber tratar bounce, no messageId e log
}

type InfoBounce struct {
	Url       string
	Parameter string
}

type DkimData struct {
	Domain     string // dominio que vai ser utilizado para assinar dkim
	PrivateKey string //chave privada, para fazer a assinatura
	Selector   string // selector utilizado na entrada de dkim no dns
}

type ContactData struct {
	Code   string            //codigo do contato, utilizado para saber tratar bounce, no messageId e log
	Email  string            // email do contato
	Fields map[string]string //dados para serem substituidos no template
}

type TemplateData struct {
	CodeSend   string   // codigo do envio
	Fields     []string // lista de variaveis que devem ser substituidas no template
	Html       string   // html do envio
	Text       string   // versao texto do html
	ReturnPath string   // endereço de return path que deve ser utilizado no envio desse template
}

func (this *SendData) getDomain() string {
	return strings.Split(this.GetContactEmail(), "@")[1]
}

func (this *SendData) SendEmail() {
	defer recoverPanic()

	Logger.Printf(logger.INFO, "Iniciando envio para %s", this)

	if ControlSend.Block(this.GetContactEmail(), this.Template.Html) {
		Logger.Printf(logger.INFO, "Não vai fazer o envio para [%s]. Pois o conteudo já foi enviado anteriormente", this)
		return
	}

	if this.Dkim == nil {
		this.Dkim = new(DkimData)
	}

	contactMx := ""
	listMx, listIpMx := cachemx.Get(this.getDomain())
	ipOut := this.ipOut
	reverse := this.reverseIpOut
	var err error

	if this.reverseIpOut == "" {
		ipOut = GetRandonIP().String()
		reverse, err = findReverseIP(ipOut, 3)
		this.SetIpReverse(ipOut, reverse)
	}
	if err != nil {
		Logger.Printf(logger.INFO, "Não conseguiu descobrir reverso do ip [%s] setando o proprio ip para utilizar no hello. Erro [%s]", ipOut, err)
		reverse = ipOut
	}
	/*################# REMOVER ISSO QUANDO OS DADOS PARA USO DE TLS ##########################*/
	//HasCertificate = false
	/*################# REMOVER ISSO QUANDO OS DADOS PARA USO DE TLS ##########################*/

	listtcpipmx, mxseq, err := createTCPAddrs(listIpMx)
	if err != nil {
		time.Sleep(time.Second * 5)
		this.ReinsertContact()
		Logger.Printf(logger.WARN, "Erro ao enviar e-mail para %s. Erro [%s]", this, err)
		return
	}

	tcpipout, err := resolveIpOut(ipOut)
	if err != nil {
		time.Sleep(time.Second * 5)
		this.ReinsertContact()
		Logger.Printf(logger.WARN, "Erro ao enviar e-mail para %s. Erro [%s]", this, err)
		return
	}
	var (
		client  *smtp.Client
		connTCP *net.TCPConn
	)

	// Parte mais demorada, limitado para 4 ips no maximo
	var errSMTP error
	var totalTryConn int
	for priori, tcpipmx := range listtcpipmx {

		if totalTryConn > 0 && turnoff.IsShutdown() {
			Logger.Printf(logger.INFO, "Sistema em desligamento, abortando envio %s", this)
			break
		}
		// Se o total de tentativas for maior que 4 é encerrada as tentativas
		if totalTryConn >= 4 {
			break
		}

		conn, c, err := newSmtpClient(tcpipout, tcpipmx, reverse, time.Minute*2)
		if err != nil {
			errSMTP = err
			totalTryConn++
			continue
		}

		errSMTP = nil
		client = c
		connTCP = conn
		if len(mxseq) > 0 && mxseq[priori] >= 0 && mxseq[priori] < len(listMx) {
			contactMx = strings.Trim(listMx[mxseq[priori]].Host, ".")
		}
		break
	}
	//valida se o dominio do mx está na blacklist
	// se estiver já marca bounce de dominio invalido
	if cachemx.CheckBlacklist(contactMx) {
		Logger.Printf(logger.INFO, "Bounce manual para o email e-mail [%s] pois o endereço de mx [%s] esta na blacklist", this, contactMx)
		this.CaptureBounce(fmt.Errorf("601 domain does not exist"))
		return
	}

	/*//desvio do hotmail
	if strings.Contains(contactMx, ".outlook.com") {
		if this.MaxRetry <= 2 {
			Logger.Printf(logger.INFO, "Desvio manual para o email e-mail [%s] pois o endereço de mx [%s] esta na blacklist", this, contactMx)
			//	time.Sleep(time.Second * 10)

			this.ReinsertContact()
			return
		}
	}
	*/
	Logger.Printf(logger.INFO, "Enviando e-mail [%s] para o endereço de mx [%s]", this, contactMx)
	if client == nil {
		if errSMTP == nil {
			errSMTP = fmt.Errorf("Servidor Indisponível")
		}
	}
	if this.CaptureBounce(errSMTP) {
		return
	}

	// caso o quit falhe, fecha mesmo assim
	defer connTCP.Close()
	// Antes de iniciar o processo de envio, valida se ainda nao chegou no limite
	if ControlLimit.CheckLimit(contactMx, ipOut) {
		//se já atingiu o limite da uma folga de 10 segundos e reinsere na fila para tentar novamente daqui a pouco
		time.Sleep(time.Second * 10)
		this.ReinsertContact()
		return
	}

	//faz o Hello com o reverso do ip de saida, para se identificar de forma correta para o provedor
	if this.CaptureBounce(client.Hello(reverse)) {
		return
	}

	// Envia TLS apenas para os principais provedores
	// O método StartTLS já faz o Helo/Ehlo
	//TO-DO verificar carregamento de certificado
	if sendUsingTls(contactMx) {
		config := &tls.Config{
			Certificates: []tls.Certificate{Certificate},
			ServerName:   contactMx,
		}
		Logger.Printf(logger.INFO, "vai fazer o StartTLS: %v - %s", err, this)
		err = client.StartTLS(config)
		if err != nil {
			Logger.Printf(logger.INFO, "Erro no StartTLS: %v - %s", err, this)
		}
	}

	if this.CaptureBounce(client.Mail(this.GetReturnPath())) {
		return
	}

	// Set the sender and recipient.
	if this.CaptureBounce(client.Rcpt(this.GetContactEmail())) {
		return
	}

	// Send the email body.
	writecloser, err := client.Data()
	if this.CaptureBounce(err) {
		return
	}

	// Monta o Email e envia o conteudo
	emailcontent := this.mountMail()
	if _, err = writecloser.Write([]byte(emailcontent)); this.CaptureBounce(err) {
		return
	}

	if this.CaptureBounce(writecloser.Close()) {
		return
	}

	if this.CaptureBounce(client.Quit()) {
		return
	}

	Logger.Printf(logger.INFO, "SENT E-mail enviado para %s", this)
}

//monta o corpo do e-mail e também os headers necessários
func (this *SendData) mountMail() string {
	dateNow := time.Now()
	email := NewEmail()
	email.AddHeader(DATE, dateNow.Format(FORMATDATE))

	// Reply-to
	if this.ReplyTo != "" {
		email.AddHeader(REPLYTO, this.ReplyTo)
	}

	//define o message id
	email.AddHeader(MESSAGEID, this.GetMessageId())

	//	email.AddHeader("List-Unsubscribe-Post","List-Unsubscribe=One-Click")
	//  email.AddHeader("List-Unsubscribe","<http://"+config.ReturnPath+"/unsubscribe/"+this.CodeSend+">")

	//feedback id utilizado principalemente par ao gmail
	email.AddHeader(FEEDBACK_ID, strings.ToLower(this.TypeSend+this.CodeSend+":"+this.CodeSend+":htech"))

	//define o return path
	email.AddHeader(RETURNPATH, this.GetReturnPath())

	//define o from do e-mail
	email.AddHeader(FROM, this.GetFrom())

	// Subject
	email.AddHeader(SUBJECT, this.Subject)

	// Contact
	email.AddHeader(TO, this.GetContactEmail())

	email.SetHtml(this.Template.Html)
	email.SetText(this.Template.Text)

	boundary := randomBoundary()
	email.SetFixedBoundary(boundary)

	//valida se vai precisar fazer assintura de dkim
	if this.checkDkim() {
		domainkey := strings.Trim(this.Dkim.Domain, BREAKLINE)
		email.AddHeader(SIGNEDBY, domainkey)
		email.DKIM.SetSelector(strings.Trim(this.Dkim.Selector, BREAKLINE))
		email.DKIM.SetPrivateKey(strings.Trim(this.Dkim.PrivateKey, BREAKLINE))
		email.DKIM.SetDomain(domainkey)
		email.DKIM.SetDate(dateNow)
	}

	return email.Build(this.Contact.Fields)
}

//Retorna o e-mail do destinatirio
func (this *SendData) GetContactEmail() string {
	return this.Contact.Email
}

//retorna o from do e-mail formatado: nome<emaill>
func (this *SendData) GetFrom() string {
	return this.SenderName + "<" + this.SenderEmail + ">"
}

func (this *SendData) checkDkim() bool {
	if this.Dkim.Domain == "" || this.Dkim.Selector == "" || this.Dkim.PrivateKey == "" {
		return false
	}
	return true
}

//forçado a retorna vazio, pois estava dando problema de entrega no gmail.
// TODO: descobrir o motivo e corrigir.
func (this *SendData) GetReturnPath() string {
	if this.Template.ReturnPath != "" {
		return this.Template.ReturnPath
	}
	return PREFIX_RETURN_PATH + "." + this._internalAddress() + "@" + config.ReturnPath
}

//retorna o valor para setar o message id do header, no seguinte formato
func (this *SendData) GetMessageId() string {
	return "<" + this._internalAddress() + "@" + this.reverseIpOut + ">"
}

//tipo.codigo.unix@
func (this *SendData) _internalAddress() string {
	return fmt.Sprintf("%s.%s.%d", this.TypeSend, this.CodeSend, time.Now().UnixNano())
}

//valida se deu erro ou nao, em caso de erro faz o tratamento
// se nao deu erro entao retorna false
// em caso de erro coloca na fila de tratamento de erros para agilizar o processo de envio
func (this *SendData) CaptureBounce(err error) bool {

	if err == nil {
		return false
	}
	bounce := &BounceData{
		CodeSend:     this.CodeSend,
		BounceText:   err.Error(),
		SendBounceTo: this.BounceRequest,
	}
	//classifica o erro
	bounce.bounceClassifier()

	QueueSend.AddBounce(bounce)
	Logger.Printf(logger.INFO, "BOUNCE Falha ao enviar e-mail, erro [%s]. %s", err, this)
	return true
}

//monta o texto padrão para gravar no log
func (this *SendData) String() string {
	//se é a primeira vez que a função é chamada entao tem que montar o texto
	if this.infolog == "" {
		this.infolog = fmt.Sprintf("envio [%s] - tipo [%s] - email [%s]", this.CodeSend, this.TypeSend, this.Contact.Email)
	}
	return this.infolog
}
func (this *SendData) SetIpReverse(ip, reverse string) {
	this.reverseIpOut = reverse
	this.ipOut = ip
}

func (this *SendData) ReinsertContact() {
	this.MaxRetry++
	QueueSend.AddContact(this)
}

func (this *SendData) IsReinsert() bool {
	return this.MaxRetry > 0
}
