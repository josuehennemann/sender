package main

import (
	"encoding/json"
	"net/http"
)

/*
| Tipo de erro      | Significado                                                     |
| ----------------- | --------------------------------------------------------------- |
| invalid_parameter | Quando algum parâmetro passado está incorreto/faltando.         |
| action_forbidden  | Quando o usuário não tem permissão para fazer determinada ação. |
| internal_error    | Quando algum erro interno em nosso servidor ocorreu.            |
| not_found         | Quando o recurso procurado não foi encontrado/não existe.       |

| Código | Significado                                                                                                           |
| ------ | --------------------------------------------------------------------------------------------------------------------- |
| 200    | Tudo ocorreu como deveria e sua requisição foi processada com sucesso.                                                |
| 201    | Tudo ocorreu como deveria e sua requisição foi processada com sucesso. Mas não tem conteúdo para retornar             |
| 400    | Algum parâmetro obrigatório não foi passado, ou os parâmetros passados não estão corretos.                            |
| 401    | Falta de autorização para acessar este endpoint.                                                                      |
| 404    | Endpoint não encontrado, revise a URL passada.                                                                        |
| 500    | Erro interno do Pagar.me, tente sua requisição novamente. Caso o erro continue, entre em contato com suporte@pagar.me |


*/

var (
	ErrorHttpForbidden     = &ErrorHttp{Message: "Usuário sem permissão", Type: "action_forbidden", httpCode: http.StatusUnauthorized}
	ErrorHttpInternalError = &ErrorHttp{Message: "Ocorreu um erro interno", Type: "internal_error", httpCode: http.StatusInternalServerError}
	ErrorHttpNotFound      = &ErrorHttp{Message: "Recurso não encontrado", Type: "not_found", httpCode: http.StatusNotFound}
)

type InterfaceHttpReturn interface {
	Json() string
	GetHttpCode() int
}
type SuccessHttp struct {
	Code     string `json:"code"`
	httpCode int    //co que deve ser utilizado no retorno do http
}

func NewSuccessHttp200(c string) *SuccessHttp {
	return &SuccessHttp{Code: c, httpCode: http.StatusOK}
}

func (s *SuccessHttp) Json() string {
	slc, _ := json.Marshal(s)
	return string(slc)
}
func (s *SuccessHttp) GetHttpCode() int {
	return s.httpCode
}

type ErrorHttp struct {
	Message       string `json:"message"`
	Type          string `json:"type"`
	ParameterName string `json:"parameter_name,omitempty"`
	httpCode      int    //codigo que deve ser utilizado no retorno do http
}

//retorna um erro dizendo que o parametro nao foi enviado
func NewErrorHttpParameterNotFound(name string) *ErrorHttp {
	e := &ErrorHttp{
		Message:       name + " está faltando",
		Type:          "invalid_parameter",
		ParameterName: name,
		httpCode:      http.StatusBadRequest,
	}
	return e
}

//retorna um erro dizendo que o conteúdo de um parametro é inválido
func NewErrorHttpInvalidParameter(name string) *ErrorHttp {
	e := &ErrorHttp{
		Message:       name + " é inválido",
		Type:          "invalid_parameter",
		ParameterName: name,
		httpCode:      http.StatusBadRequest,
	}
	return e
}
func (e *ErrorHttp) Json() string {
	slc, _ := json.Marshal(e)
	return string(slc)
}
func (e *ErrorHttp) GetHttpCode() int {
	return e.httpCode
}

//struct que recebe os dados para envio de e-mail
type ReceiverSendMail struct {
	To          string                   `json:"to"`
	Subject     string                   `json:"subject"`
	ReplyTo     string                   `json:"reply_to"`
	SenderName  string                   `json:"sender_name"`
	SenderEmail string                   `json:"sender_email"`
	Content     *ReceiverSendMailContent `json:"content"`
	Bounce      *ReceiverSendMailContent `json:"bounce"`
	Type        string                   //esse campo não vem no json, ele é setado internamente
}

type ReceiverSendMailContent struct {
	Html      string `json:"html,omitempty"`      //usado no content
	Text      string `json:"text,omitempty"`      //usado no content
	Url       string `json:"url,omitempty"`       //usado no bounce
	Parameter string `json:"parameter,omitempty"` //usado no bounce
}

func (this *ReceiverSendMail) Validate() (e *ErrorHttp) {
	//valida o to
	if e = this.checkTo(); e != nil {
		return e
	}
	//valida o subject
	if e = this.checkSubject(); e != nil {
		return e
	}
	// valida o content
	if e = this.checkContent(); e != nil {
		return e
	}

	// valida o content
	if e = this.checkBounce(); e != nil {
		return e
	}

	return nil
}

//funções "internas" de validação
func (this *ReceiverSendMail) checkTo() *ErrorHttp {
	if this.To == "" {
		return NewErrorHttpParameterNotFound("to")
	}
//	if !isEmail(this.To) {
//		return NewErrorHttpInvalidParameter("to")
//	}
	return nil
}

func (this *ReceiverSendMail) checkSubject() *ErrorHttp {
	if this.Subject == "" {
		return NewErrorHttpParameterNotFound("subject")
	}
	if len(this.Subject) <= 5 {
		return NewErrorHttpInvalidParameter("subject")
	}
	return nil
}

func (this *ReceiverSendMail) checkContent() *ErrorHttp {
	if this.Content == nil {
		return NewErrorHttpParameterNotFound("content")
	}

	if len(this.Content.Html) <= 20 {
		return NewErrorHttpInvalidParameter("html")
	}
	/*if len(this.Content.Text) <= 20 {
		return NewErrorHttpInvalidParameter("text")
	}*/

	return nil
}
func (this *ReceiverSendMail) checkBounce() *ErrorHttp {
	return nil //retirado validação de bounce, pois tem envios que é só descartar o bounce
	if this.Bounce == nil {
		return NewErrorHttpParameterNotFound("url")
	}

	if len(this.Bounce.Url) <= 10 {
		return NewErrorHttpInvalidParameter("url")
	}
	return nil
}
