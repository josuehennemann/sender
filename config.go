package main

import (
	"github.com/josuehennemann/yaml"
	"io/ioutil"
)

type Config struct {
	LogPath        string `yaml:"logPath"`     // path do arquivo de log
	DataPath       string `yaml:"dataPath"`    // path do diretorio data da aplicação
	HttpAddress    string `yaml:"httpAddress"` //endereço http
	DkimDomain     string `yaml:"dkim_domain"`
	DkimSelector   string `yaml:"dkim_selector"`
	DkimPrivateKey string `yaml:"dkim_privateKey"`
	Authorization  string `yaml:"authorization"`
	ReturnPath     string `yaml:"returnPath"`
}

func initConfig() error {
	config = &Config{}
	data, err := ioutil.ReadFile(*fileConf)
	yaml.Unmarshal(data, config)
	return err
}
