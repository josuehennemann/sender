package main

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/josuehennemann/html2text"
	"github.com/josuehennemann/logger"
	"io"
	"io/ioutil"
	"mime"
	"mime/multipart"
	"mime/quotedprintable"
	"net/mail"
	"regexp"
	"strings"
)

type EmailStatus struct {
	basicCode string
	text      string
}

const (
	EMAIL_RETURN_DELIVERY    = "delivery"
	ORIGINAL_MESSAGE         = "----- Original message -----"
	ORIGINAL_MESSAGE_HEADERS = "Original message headers:"
	BELOW_THIS_LINE          = "--- Below this line is a copy of the message."
	THIS_COPY_MESSAGE        = "------ This is a copy of the message, including all the headers. ------"
	CABECALHO_DO_EMAIL       = "------------------------- CABE"
)

var (
	RegexpStatusCode              = regexp.MustCompile("\n[Ss]tatus: ([45])\\.([0-9]\\.[0-9])")
	RegexpDiagnosticCode          = regexp.MustCompile("[Dd]iagnostic-[Cc]ode: ([^\\n]*)")
	RegexpDiagnosticCodeDontBegin = regexp.MustCompile("^[^;:]*[;:]")

	regexpFindErrorTextPlain  = regexp.MustCompile("host [^:]*:(?:<.*>[:]?)?[[:space:]]*([0-9]*)[\\-[[:space:]]]*([45])\\.([0-9]\\.[0-9])")
	regexpFindErrorTextPlain2 = regexp.MustCompile("\\(#?([45])\\.([0-9]\\.[0-9])\\)?")
	regexpFindErrorTextPlain3 = regexp.MustCompile("<<< ([0-9]*)[\\-[[:space:]]]*([45])\\.([0-9]\\.[0-9])")
	regexpFindErrorTextPlain4 = regexp.MustCompile("responded with failure: ([0-9]*)[\\-[[:space:]]]*([45])\\.([0-9]\\.[0-9])")

	validateRCPT = regexp.MustCompile(`^(return.*)`)
)

func initServerSMTP() {
	s := &ServerSMTP{
		Addr:      ":25",
		OnNewMail: onNewMail,
	}
	checkErrorAndKillMe(s.ListenAndServe())
}

type env struct {
	*BasicEnvelope
	body bytes.Buffer
}

//Nesta função vamos implementar todas as validaçoes para aceitar o e-mail
func (e *env) AddRecipient(rcpt MailAddressSMTP) error {
	if !validateRCPT.MatchString(rcpt.Email()) {
		return SMTPError("554 5.5.1 Error: no valid recipients")
	}
	return e.BasicEnvelope.AddRecipient(rcpt)
}

func onNewMail(c ConnectionSMTP, from MailAddressSMTP) (Envelope, error) {
	return &env{new(BasicEnvelope), bytes.Buffer{}}, nil
}

//Esta função vai recebendo linha a linha do body
func (e *env) Write(line []byte) error {
	e.body.Write(line)
	return nil
}

//a função close sabe que terminou de ler o body do e-mail e entao compacta o conteudo e coloca em uma fila que vai processar depois
func (e *env) Close() error {

	//compacta o conteudo do e-mail
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)
	var err error
	if _, err = gz.Write(e.body.Bytes()); err != nil {
		Logger.Printf(logger.WARN, "Falha na compactação, do e-mail de retorno, durante o Write. Erro [%s], body [%s]", err, e.body.String())
		return nil
	}
	if err = gz.Flush(); err != nil {
		Logger.Printf(logger.WARN, "Falha na compactação, do e-mail de retorno, durante o Flush. Erro [%s], body [%s]", err, e.body.String())
		return nil
	}
	if err = gz.Close(); err != nil {
		Logger.Printf(logger.WARN, "Falha na compactação, do e-mail de retorno,durante o Close. Erro [%s], body [%s]", err, e.body.String())
		return nil
	}

	m := &MailQueue{Data: b.Bytes(), MailAddress: e.BasicEnvelope.rcpts[0].Email()}
	Logger.Printf(logger.INFO, "Recebeu e-mail de bounce [%s]", m.MailAddress)
	QueueSend.AddMailReturn(m)
	return nil
}

type MailQueue struct {
	Data        []byte        //dados do e-mail (des)compactados
	MailAddress string        //e-mail do destinatario (ou seja o endereço em que recebemos o e-mail)
	message     *mail.Message //e-mail "parseado" pelo go
}

// método que faz o parse do e-mail recebido e trata o bounce
func (mq *MailQueue) parseEmail() {
	var err error
	var buf bytes.Buffer
	buf.Write(mq.Data)
	zr, _ := gzip.NewReader(&buf)
	bufOut := new(bytes.Buffer)

	if _, err = io.Copy(bufOut, zr); err != nil {
		Logger.Printf(logger.WARN, "Erro ao tentar descompactar conteudo de e-mail de retorno. Erro [%s]")
		return
	}

	mq.message, err = mail.ReadMessage(bufOut)
	if err != nil {
		Logger.Printf(logger.INFO, "Falha no parse da mensagem do e-mail de retorno: %s [%s]", mq, err.Error())
		return
	}

	mediaType, params, err := mime.ParseMediaType(mq.message.Header.Get("Content-Type"))
	//caso nao consiga identificar o media type do e-mail, entao vamos assumir que é text/plain
	if err != nil && err.Error() == "mime: no media type" {
		err = nil
		mediaType = "text/plain"
	}

	if err != nil {
		Logger.Printf(logger.INFO, "Erro ao tentar fazer parse do mime type do e-mail de retorno. Mensagem [%s] Header [%s] Error [%s]", bufOut.String(), mq.message.Header, err.Error())
		return
	}

	textBounce := getBounceMessage(mediaType, params, mq.message.Body, mq.message.Header)
	if textBounce == "" {
		Logger.Printf(logger.INFO, "Não achamos texto de bounce no e-mail de bounce. %s", mq)
		return
	}
	TypeSend, CodeSend := mq.parseRcpt()
	newBounce := &SendData{
		CodeSend: CodeSend,
		TypeSend: TypeSend,
		Contact: &ContactData{
			//			Code:  ContactCode,
			Email: "RETURNPATH",
		},
	}

	newBounce.CaptureBounce(errors.New(textBounce))

}

//return.tipo.codigo.unix@
func (mq *MailQueue) parseRcpt() (TypeSend, CodeSend string) {
	info := strings.Split(strings.Split(mq.MailAddress, "@")[0], ".")
	//info[0]  => return
	//info[1]  => tipo de envio
	//info[2]  => codigo de envio
	//info[3] => é um unixnano
	return info[1], info[2]
}

func (mq *MailQueue) String() string {
	return fmt.Sprintf("Rcpt [%s]", mq.MailAddress)
}

type Header interface {
	Get(string) string
}

func getBounceMessage(mediaType string, params map[string]string, body io.Reader, header Header) (error_msg string) {

	switch {
	case strings.Contains(mediaType, "message/delivery-status"):
		data, err := ioutil.ReadAll(body)
		if err != nil {
			Logger.Printf(logger.INFO, "Falha ao ler body do e-mail recebido. Erro [%s]", err)
			return
		}
		//é o unico que nao chama a funcao findAndParseTextError pq ele tem um tratamento diferente
		content := string(data)
		diagnosticCode, statusCode, found := findErrorInText(&content)

		if statusCode == EMAIL_RETURN_DELIVERY {
			return
		}

		if !found {
			matches := RegexpStatusCode.FindStringSubmatch(string(content))
			if len(matches) <= 1 {
				return
			}
			statusCode = matches[2]
		}

		if diagnosticCode == "" {
			diagnosticCode = getDiagnosticCode(string(content))

			if _, ok := StatusCodeEnhanced[statusCode]; !ok {
				statusCode = "nf"
			}

			diagnosticCode = StatusCodeEnhanced[statusCode].basicCode
		}

		error_msg = diagnosticCode + " - " + StatusCodeEnhanced[statusCode].text

	case strings.Contains(mediaType, "text/html"):

		data, err := ioutil.ReadAll(body)
		if err != nil {
			Logger.Printf(logger.INFO, "Falha ao ler body do e-mail recebido. Erro [%s]", err)
			return
		}
		data = parseContentTransferEncoding(header.Get("Content-Transfer-Encoding"), data)
		content := html2text.HTML2Text(string(data))
		error_msg = findAndParseTextError(&content)

	case strings.Contains(mediaType, "text/plain"):
		reader := bufio.NewReader(body)

		headerContent := new(bytes.Buffer)
		emailContent := new(bytes.Buffer)
		isBody := false
		firstLine := true
		for {
			line, _, err := reader.ReadLine()
			if err != nil {
				break
			}

			if strings.Contains(string(line), ORIGINAL_MESSAGE) ||
				strings.Contains(string(line), ORIGINAL_MESSAGE_HEADERS) ||
				strings.Contains(string(line), BELOW_THIS_LINE) ||
				strings.Contains(string(line), THIS_COPY_MESSAGE) ||
				strings.Contains(string(line), CABECALHO_DO_EMAIL) {
				isBody = true
				continue
			}

			if !isBody {
				headerContent.Write(append(line, []byte("\n")...))
				continue
			}
			if firstLine {
				if string(line) == "" {
					continue
				}
				firstLine = false
			}
			emailContent.Write(append(line, []byte("\n")...))
		}

		if headerContent.Len() > 0 {

			data := parseContentTransferEncoding(header.Get("Content-Transfer-Encoding"), headerContent.Bytes())
			headerContent.Reset()
			headerContent.Write(data)
			content := headerContent.String()
			error_msg = findAndParseTextError(&content)

		}

	case strings.Contains(mediaType, "multipart/"):
		mp := multipart.NewReader(body, params["boundary"])
		for {
			part, err := mp.NextPart()
			if err == io.EOF {
				break
			}

			if err != nil {
				Logger.Printf(logger.ERROR, "Falha ao ler o conteudo do multipart boundary[%s]. Error[%s]", params["boundary"], err)
				break
			}

			contentPart, err := ioutil.ReadAll(part)
			if err != nil {
				Logger.Printf(logger.ERROR, "Falha no parse da mensagem read all do multipart. Error[%s]", err)
				continue
			}

			mediaType, params, err = mime.ParseMediaType(part.Header.Get("Content-Type"))
			if err != nil {
				Logger.Printf(logger.ERROR, "Falha no get do content-type da mensagem do multipart. Conteudo [%s]. Error [%s]", string(contentPart), err)
				break
			}

			error_msg = getBounceMessage(mediaType, params, bytes.NewBuffer(contentPart), part.Header)
			//se já achou o erro entao nao precisa ler o resto do multipart
			if error_msg != "" {
				break
			}
		}
	default:
		Logger.Printf(logger.INFO, "Content-Type não tratado [%s]", mediaType)

	}

	return
}

func parseContentTransferEncoding(header string, data []byte) []byte {
	switch header {
	case "base64":
		return parseContentBase64(string(data))
	case "quoted-printable":
		reader := quotedprintable.NewReader(bytes.NewBuffer(data))
		buffer := new(bytes.Buffer)
		tempBuff := make([]byte, 128)
		for {
			n, err := reader.Read(tempBuff)
			if err != nil {
				break
			}
			buffer.Write(tempBuff[:n])
		}
		return buffer.Bytes()

	}
	return data
}
func findAndParseTextError(content *string) (error_msg string) {
	diagnosticCode, statusCode, found := findErrorInText(content)
	if diagnosticCode == "" {
		if _, ok := StatusCodeEnhanced[statusCode]; !ok {
			statusCode = "nf"
		}
		diagnosticCode = StatusCodeEnhanced[statusCode].basicCode
	}
	if found {
		if statusCode == EMAIL_RETURN_DELIVERY {
			return
		}

		error_msg = fmt.Sprintf("%s - %s", diagnosticCode, StatusCodeEnhanced[statusCode].text)

	}
	return
}
func findErrorInText(content *string) (string, string, bool) {

	*content = strings.ToLower(*content)

	matches := regexpFindErrorTextPlain.FindStringSubmatch(*content)
	if len(matches) >= 3 {
		return matches[1], matches[3], true
	}

	matches = regexpFindErrorTextPlain2.FindStringSubmatch(*content)
	if len(matches) >= 2 {
		return "", matches[2], true
	}

	matches = regexpFindErrorTextPlain3.FindStringSubmatch(*content)
	if len(matches) >= 2 {
		return matches[1], matches[3], true
	}

	matches = regexpFindErrorTextPlain4.FindStringSubmatch(*content)
	if len(matches) >= 2 {
		return matches[1], matches[3], true
	}

	*content = strings.Replace(*content, "\n", " ", -1)
	for _, item := range emailContentPattern {
		for _, patt := range item.patterns {
			if strings.Contains(*content, patt) {
				return "", item.code, true
			}
		}
	}

	return "", "", false

}

func getDiagnosticCode(content string) (text string) {

	matches := RegexpDiagnosticCode.FindStringSubmatch(content)
	if len(matches) <= 1 {
		text = "451"
		return
	}

	error_msg := matches[1]

	error_msg = RegexpDiagnosticCodeDontBegin.ReplaceAllString(error_msg, "")

	error_msg = strings.TrimSpace(error_msg)

	matches = regexpGetNumbers.FindStringSubmatch(error_msg)
	// Tem um proprio codigo de erro
	if len(matches) > 1 && len(matches[1]) == 3 {
		text = matches[1]
		return
	}

	return
}

func parseContentBase64(data string) []byte {

	if n := strings.Index(data, "."); n != -1 {
		data = data[0:n]
	}
	partDecoded, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		return nil
	}

	return partDecoded

}
