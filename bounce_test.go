//fonte teste de classificação dos bounces
package main

import (
	"testing"
)

func TestBounceClassifier(t *testing.T) {
	list := []struct {
		Text   string
		Expect BounceCode
	}{
		{"101 The server is unable to connect.", BOUNCE_TYPE_EU},
		{"111 Connection refused or inability to open an SMTP stream.", BOUNCE_TYPE_AU},
		{"211 System status message or help reply.", BOUNCE_TYPE_EU},
		{"214 A response to the HELP command.", BOUNCE_TYPE_EU},
		{"251 User not local will forward: the recipient’s account is not on the present server, so it will be relayed to another.", BOUNCE_TYPE_AU},
		{"252 The server cannot verify the user, but it will try to deliver the message anyway.", BOUNCE_TYPE_AU},
		{"604 Timeout connection problem: there have been issues during the message transfer.", BOUNCE_TYPE_SU},
		{"The service is unavailable due to a connection problem: it may refer to an exceeded limit of simultaneous connections, or a more general temporary problem.", BOUNCE_TYPE_EU},
		{"422 The recipient’s mailbox has exceeded its storage limit.", BOUNCE_TYPE_MF},
		{"431 Not enough space on the disk, or an “out of memory” condition due to a file overload.", BOUNCE_TYPE_MF},
		{"The recipient’s server is not responding.", BOUNCE_TYPE_SU},
		{"Bad email address.", BOUNCE_TYPE_AU},
		{"553 Requested action not taken – Mailbox name invalid. That is, there’s an incorrect email address into the recipients line.", BOUNCE_TYPE_AU},
	}

	for k, v := range list {
		if found := parseAndBounceClassifier(v.Text); found != v.Expect {
			t.Error("Falha ao classificar bounce, line ", k, "text ", v.Text, " expected ", v.Expect, " found ", found)
		}
	}
}
