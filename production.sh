CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o sender
#valida se compilou o projeto, e somente faz o scp se compilou
if [[ $? != 0 ]]; then
	echo "Falha ao compilar"
	exit 1
fi
echo "Enviando para o server..."
scp sender root@51.38.99.74:
#scp conf/sender.yaml root@51.38.99.74:
#scp system.d/sender.service root@51.38.99.74:
#systemctl stop sender.service; cp /var/lib/sprinta/sender/sender /var/lib/sprinta/sender/sender_bkp;  chmod 777 sender ; mv sender /var/lib/sprinta/sender/ ; systemctl start sender.service