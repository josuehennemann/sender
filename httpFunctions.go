package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/josuehennemann/logger"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

//Fecha o Body da requição e garante que vai fechar o ponteiro do request
func PrepareClose(w http.ResponseWriter, r *http.Request) {
	r.Close = true
	r.Body.Close()
	return
}

//listen de teste, recebe apenas 3 parametros: e-mail, versao html e versao texto do e-mail
func SendTestHttp(w http.ResponseWriter, r *http.Request) {
	defer PrepareClose(w, r)
	w.Header().Set("Content-Type", "text/plain")
	email := r.FormValue("email")
	//htmlVersion := r.FormValue("html_version")
	//textVersion := r.FormValue("text_version")
	Logger.Printf(logger.INFO, "Enviando e-mail para o endereço [%s]", email)
	if email == "" {
		io.WriteString(w, "E-mail invalido")
		return
	}

	dkimHennemannTech := &DkimData{
		Domain: "hennemanntech.com",
		PrivateKey: `-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQC52heMbMQRQoQ8nADL7ejDNNtk1fsvHTGQLctdrTX/i6xcLWAw
Gl3YZLvUl8sd8uiLDcoGs+NpPLAU2MGiVArKHYHi6h4Vkrg9itE+QKzoN4dKgsch
liCBL57MOsK+eFdwHldEoTuI2MS2Z+O3J8vMOGympcx4LNrbd7Sx9EU+tQIDAQAB
AoGACV9XOqytgpVRJ+FSJPade2KqAb7MyQZ23meAzvuSMo3vpkJfjG13+Ba0Zjc1
dhAu6/xJCOd/e2olExgPQ8lrNcoPrsanLBzUiu8b9ClhEG6JWaVMRnydY0Hjgtzv
rO5Xy+Xrd+7btFqNvPI/FXq0aWCxNyL4NLczpa0sQ7+4uWECQQDr+sAKeG7YkjI9
bLSj6XnJfhT7BsWYv/XeqfNNKisq4DJWXdBqPwPTrm2VuC6MHGpRn3Mz5w2fklAi
uYIfyKBNAkEAyZ6eZM1UAFpE9VTK4k21kUvi/KRp3QJdXKurHZQX1R3V7nFe90Qm
9yMuaKtceSWnrvxPFU6NOL+HoL11Y4QMCQJAYp6ntXiBcrcekfclQz9m62Nq50H8
QIsw+P5ztPMDmWabAucwG8b8FXJXT/PqWBnkAPqvTaipU9/05545mIjjHQJBALfb
OI+tmgN0Tdv2hciWmpMzw3slpxAOXvrp5PHlIVKLkZGNus77TBkj8OcsFPi4uM/e
b3N7nVZ5egG/ozOrr5kCQCbikupGv6ponZp3cKaGKIW2nHi5DUZOCDpAblyyi5Ku
FQzPYdb4wR6pHWhxt6zqG8lZKdbyMh4xAaZePoQe1zY=
-----END RSA PRIVATE KEY-----`,
		Selector: "123456789",
	}

	data := &SendData{
		ClientCode:  "1",
		CodeSend:    "1",
		Subject:     "Teste de envio",         //assunto do envio
		SenderEmail: "contato@sprinta.com.br", // e-mail do remetente
		SenderName:  "Sprinta",                // nome do rementente
		Contact: &ContactData{
			Email: email,
			Code:  "1",
		}, //Dados do contato
		Template: &TemplateData{
			CodeSend: "1",
			Html:     "Primeiro teste de envio",
			Text:     "Primeiro teste de envio",
			//Html:     "mail.myserver.com[X.X.X.X] responded with failure: 572-4.0.3", // texto utilizado para enviar para o bounce para o proprio sender
			//Text:     "mail.myserver.com[X.X.X.X] responded with failure: 572-4.0.3",
		},
		Dkim: dkimHennemannTech,
	}
	QueueSend.AddContact(data)

	io.WriteString(w, "E-mail incluido na fila de envio")
}

//Url que recebe os dados para envio de e-mail transacional
func SendMailTRHttp(w http.ResponseWriter, r *http.Request) {
	defer PrepareClose(w, r)
	t := time.Now()
	w.Header().Set("Content-Type", "Application/json")
	var (
		resp      InterfaceHttpReturn
		requestID string
	)
	if !CheckAuthorization(r) || r.Method != http.MethodPost {
		resp = ErrorHttpForbidden
	} else {
		resp, requestID = buildSendMail(r, EMAIL_TYPE_TR)
	}

	codeHttp := resp.GetHttpCode()
	w.WriteHeader(codeHttp)
	io.WriteString(w, resp.Json())

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	Logger.Printf(logger.INFO, "Processou a requisição [%s] em %0.6f. Http [%d], IP: [%s] ", requestID, time.Since(t).Seconds(), codeHttp, ip)
}

func buildSendMail(r *http.Request, emailType string) (InterfaceHttpReturn, string) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		Logger.Printf(logger.WARN, "Falha ao ler o body da requisição, erro [%s]", err)
		return ErrorHttpInternalError, "0"
	}
	buffer := new(bytes.Buffer)
	if err = json.Compact(buffer, body); err != nil {
		Logger.Printf(logger.WARN, "Falha ao normalizar json da requisição, erro [%s] body [%s]", err, body)
		return ErrorHttpInternalError, "0"
	}
	body = buffer.Bytes()

	t := &ReceiverSendMail{}
	err = json.Unmarshal(body, t)
	if err != nil {
		Logger.Printf(logger.WARN, "Falha ao fazer unmarshal da requisição, erro [%s] body [%s]", err, body)
		return ErrorHttpInternalError, "0"
	}

	//valida se os dados postados estao corretos
	if e := t.Validate(); e != nil {
		return e, "0"
	}
	//pega o id unico da requisição de acordo com o tipo de envio
	var counter *Counter
	switch emailType {
	case EMAIL_TYPE_TR:
		counter = CounterTR
	case EMAIL_TYPE_BL:
		counter = CounterBL
	case EMAIL_TYPE_NW:
		counter = CounterNW
	}
	requestID := counter.GetNextValueString()
	// monta a struct de retorno função
	success := NewSuccessHttp200(requestID)
	
	if t.Bounce == nil {
		t.Bounce = &ReceiverSendMailContent{}
	}

	//monta a struct que vai ser incluida na fila de envio
	data := &SendData{
		CodeSend:    requestID,     //Define o codigo do envio como o requestId
		TypeSend:    emailType,     //define o tipo do envio
		Subject:     t.Subject,     //assunto do envio
		ReplyTo:     t.ReplyTo,     // endereço de retorno
		SenderEmail: t.SenderEmail, // e-mail do remetente
		SenderName:  t.SenderName,  // nome do rementente
		Contact: &ContactData{
			Email: t.To,
		}, //Dados do contato
		Template: &TemplateData{
			CodeSend: requestID,
			Html:     t.Content.Html,
			Text:     t.Content.Text,
		},
		BounceRequest: &InfoBounce{
			Url:       t.Bounce.Url,
			Parameter: t.Bounce.Parameter,
		},
	}

	//se não veio nenhum dkim na requisição entao usa o padrao do sender
	//no momento sempre vai usar o padrão, já deixei assim para manter flexivel para implementações futuras
	if data.Dkim == nil {
		data.Dkim = StandardDkim
	}
	if emailType == EMAIL_TYPE_NW {
		QueueSend.AddContactNow(data)
	} else {
		QueueSend.AddContact(data)
	}
	return success, requestID
}

//valida se tem token de Authorization no conf, e se ele é igual ao que veio na requisição
func CheckAuthorization(r *http.Request) bool {
	return config.Authorization != "" && r.Header.Get("Authorization") == config.Authorization
}

func ErrorHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "Application/json")
	w.WriteHeader(ErrorHttpNotFound.GetHttpCode())
	io.WriteString(w, ErrorHttpNotFound.Json())
	Logger.Printf(logger.INFO, "Page not found %s;%s;%s;", r.RemoteAddr, r.URL.Path, r.Method)
}

func SimulateSendBounceHttp(w http.ResponseWriter, r *http.Request) {
	defer PrepareClose(w, r)
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusOK)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		io.WriteString(w, "Falha no body")
		return
	}
	buffer := new(bytes.Buffer)
	if err = json.Compact(buffer, body); err != nil {
		io.WriteString(w, "Falha no josn")
		return
	}
	body = buffer.Bytes()

	t := &ReceiverSendMail{}
	err = json.Unmarshal(body, t)
	if err != nil {
		io.WriteString(w, "Falha no unmarshal")
		return
	}

	if t.Bounce.Url == "" {
		io.WriteString(w, "Url invalida")
		return
	}

	bounce := &BounceData{
		CodeSend:     "SS01",
		BounceText:   "421 4.7.0 [TSS04] Messages from 51.38.99.74 temporarily deferred due to user complaints - 4.16.55.1; see https://help.yahoo.com/kb/postmaster/SLN3434.html",
		SendBounceTo: &InfoBounce{Url: t.Bounce.Url},
	}
	//classifica o erro
	bounce.bounceClassifier()

	io.WriteString(w, "Bounce enviado: "+fmt.Sprintf("%v", bounce.SendRequest()))
}

//Url que recebe os dados para envio de e-mail transacional
func SendMailNWHttp(w http.ResponseWriter, r *http.Request) {
	defer PrepareClose(w, r)
	t := time.Now()
	w.Header().Set("Content-Type", "Application/json")
	var (
		resp      InterfaceHttpReturn
		requestID string
	)
	if !CheckAuthorization(r) || r.Method != http.MethodPost {
		resp = ErrorHttpForbidden
	} else {
		resp, requestID = buildSendMail(r, EMAIL_TYPE_NW)
	}

	codeHttp := resp.GetHttpCode()
	w.WriteHeader(codeHttp)
	io.WriteString(w, resp.Json())

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	Logger.Printf(logger.INFO, "Processou a requisição de envio imediato [%s] em %0.6f. Http [%d], IP: [%s] ", requestID, time.Since(t).Seconds(), codeHttp, ip)
}
