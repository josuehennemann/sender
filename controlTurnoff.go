package main

import (
	"github.com/josuehennemann/logger"
	"sync"
)

const (
	SYSTEM_STATE_ON      = 1 << iota // 1
	SYSTEM_STATE_OFF                 // 2
	SYSTEM_STATE_PAUSING             // 4
)

type TurnOffSystem struct {
	status     int            //status do sistema
	send       sync.WaitGroup //wg de envios
	bounce     sync.WaitGroup //wg de bounces
	receiver   sync.WaitGroup //wg de controle de entrada dos dados
	mailReturn sync.WaitGroup //wg de controle dos e-mails de return path
}

func initTurnOff() *TurnOffSystem {
	t := &TurnOffSystem{}
	t.status = SYSTEM_STATE_ON
	t.send = sync.WaitGroup{}
	t.bounce = sync.WaitGroup{}
	t.receiver = sync.WaitGroup{}
	t.mailReturn = sync.WaitGroup{}
	Logger.Printf(logger.INFO, "Inicializou a estrutura de controle de desligamento do sistema")
	return t
}

// Lista de metodos que adiciona e remove itens dos wg
func (turn *TurnOffSystem) AddSend() {
	turn.send.Add(1)
}
func (turn *TurnOffSystem) DoneSend() {
	turn.send.Done()
}
func (turn *TurnOffSystem) AddBounce() {
	turn.bounce.Add(1)
}
func (turn *TurnOffSystem) DoneBounce() {
	turn.bounce.Done()
}
func (turn *TurnOffSystem) AddReceiver() {
	turn.receiver.Add(1)
}
func (turn *TurnOffSystem) DoneReceiver() {
	turn.receiver.Done()
}
func (turn *TurnOffSystem) AddMailReturn() {
	turn.mailReturn.Add(1)
}
func (turn *TurnOffSystem) DoneMailReturn() {
	turn.mailReturn.Done()
}

//Executa o wait dos waitgroups do sistema
func (turn *TurnOffSystem) Wait() {
	turn.send.Wait()
	turn.bounce.Wait()
	turn.receiver.Wait()
	turn.mailReturn.Wait()
}

//altera o status do sistemas
func (turn *TurnOffSystem) SetSystemState(s int) {
	turn.status = s
}

//verifica se o sistema esta em desligamento
func (turn *TurnOffSystem) IsShutdown() bool {
	return turn.checkState(SYSTEM_STATE_OFF | SYSTEM_STATE_PAUSING)
}

//verifica se o sistema já esta up
func (turn *TurnOffSystem) IsAlready() bool {
	return turn.checkState(SYSTEM_STATE_ON)
}

//função interna que valida o status
func (turn *TurnOffSystem) checkState(s int) bool {
	return turn.status&s != 0
}
