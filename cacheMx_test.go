package main

import (
	"fmt"
	"testing"
)

func TestCacheMx(t *testing.T) {
	_init() // inicializa as variaveis globais
	cache := NewCacheMx()
	mxs, ips := cache.Get("gmail.com")
	fmt.Println(mxs, ips)
}
