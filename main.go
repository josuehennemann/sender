/*
	Envio de email:
	O e-mail deve ser enviado assinando dkim e utilizando preferencialmente com tls
OK	Montar struct para realizar o envio, nessa struct deve conter todos os dados para fazer envio.
OK		Ex: assunto, remetente, body (html e txt), etc..
OK	Implementar uso de filas de envios: A principio vamos utilizar apenas 2 filas: uma principal e outra para retentativas.
PD		Implementar numero retentativas com tempo crescente. Ex: em 5 min, depois 10, 15, 20. etc...
PD		Para saber se vamos retentar temos que saber analisar os bounces, pois existem bounces permantes e temporarios. So devemos retentar os temporarios.
OK		A analise de bounce é feita através da string de erro do servidor do destinatario.
OK	Start e stop do sistema
		Quando o sistema vai ser desligado devemos salvar em disk o cache de mx.
		Quando o sistema sobe devemos carregar para a memoria os dados do cache.
		Para isso podemos simplesmente salvar um arquivo json
LISTA DE TODO:
OK	Alterar texto e siglas dos erros para ingles
OK	Alterar no processo de envio para saber utilizar o id unico de envio que gerado pelo proprio sender
OK	Incluir novas entradas nos confs: dominio padrao para uso do dkim (dominio, selector e chave privada), dominio padrao de return-path
OK	Incluir campo de tipo de envio e url para marcar bounce na struct SendData (o tipo de envio transacional não precisa do codigo de contato)
OK	Na rotina que processa os bounce, fazer o post para a url que veio informada no envio
		Pensar em como tratar o bounce que veio pelo return path
	Criar endpoint para envio de e-mail
		Dados serao enviados em json no body OK
		validar header com key de autorização OK
		validar origem
OK	criar identificador unico de cada envio recebido, pois eles viram de origens diferentes entao pode se repetir


*/
package main

import (
	"flag"
	"fmt"
	"github.com/josuehennemann/logger"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"
)

func main() {
	//faz o parse da variaveis que sao passadas para o binario
	flag.Parse()

	initService()

	//trava a execução da main
	select {}
}

//verifica se deu erro ou nao, em caso de erro, printa o erro e mata o script
func checkErrorAndKillMe(e error) {
	if e == nil {
		return
	}
	fmt.Println(e.Error())
	os.Exit(2)
}

func setOutput() {
	dirLog := filepath.Dir(config.LogPath)
	if dirLog == "." {
		dirLog = ""
	} else {
		dirLog += "/"
	}
	daemon, err := os.OpenFile(dirLog+"sender-Daemon.log", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0777)
	checkErrorAndKillMe(err)
	syscall.Dup2(int(daemon.Fd()), 1)
	syscall.Dup2(int(daemon.Fd()), 2)
}
func initService() {
	var err error

	//inicia o processo para subir o binario como serviço no linux
	//	procDaemon, err = daemon.Daemonize(*pidPath)
	checkErrorAndKillMe(err)

	//cria a goroutine que sabe tratar o desligamento do serviço
	go killMeSignal()

	_init()

}

//inicializa as variaveis que podem ser utilizadas caso rode sem ser serviço
func _init() {
	//carrega para a memoria o arquivo de inicialização
	err := initConfig()
	checkErrorAndKillMe(err)
	setOutput()
	//inicia o arquivo de log
	Logger, err = logger.New(config.LogPath, logger.LEVEL_ALL, true)
	checkErrorAndKillMe(err)
	turnoff = initTurnOff()

	createDir()
	MyIPS, err = loadIps()
	checkErrorAndKillMe(err)
	if len(MyIPS) == 0 {
		checkErrorAndKillMe(fmt.Errorf("Server sem ips"))
	}

	Logger.Printf(logger.INFO, "Carregou [%d] ips do servidor", len(MyIPS))

	//inicia o servidor http
	go startHttpServer()
	//inicia o servidor smtp
	go initServerSMTP()

	cachemx = NewCacheMx()

	Logger.Printf(logger.INFO, "Vai gerar os certificados")

	Certificate, HasCertificate = loadCertificate()

	Logger.Printf(logger.INFO, "Carregou certificados para o starttls [%v]", HasCertificate)

	ControlLimit = InitControlLimitMX()

	Logger.Printf(logger.INFO, "Iniciou o controle de limite por mx")

	CounterTR = NewCounter("email_" + EMAIL_TYPE_TR)
	CounterBL = NewCounter("email_" + EMAIL_TYPE_BL)
	CounterNW = NewCounter("email_" + EMAIL_TYPE_NW)

	Logger.Printf(logger.INFO, "Iniciou os counters de e-mail")

	//inicio da validação de dkim
	if config.DkimDomain == "" || config.DkimPrivateKey == "" || config.DkimSelector == "" {
		checkErrorAndKillMe(fmt.Errorf("Invalid value to standard dkim"))
	}

	dkimTmp := new(DKIM)
	dkimTmp.SetPrivateKey(config.DkimPrivateKey)
	if !dkimTmp.using {
		checkErrorAndKillMe(fmt.Errorf("Invalid value to private key dkim"))
	}

	//seta a variavel com o dkim padrão do sender
	StandardDkim = &DkimData{
		Domain:     config.DkimDomain,
		PrivateKey: config.DkimPrivateKey,
		Selector:   config.DkimSelector,
	}
	// TERMINOU O DKIM
	Logger.Printf(logger.INFO, "Carregou o dkim padrão")

	QueueSend, err = NewControlQueue()
	checkErrorAndKillMe(err)
	Logger.Printf(logger.INFO, "Iniciou filas de envio")
}

func killMeSignal() {
	c := make(chan os.Signal)
	signal.Notify(c)
	for {
		s := <-c
		if s == syscall.SIGTERM || s == syscall.SIGINT {
			Logger.Printf(logger.INFO, "Sinal de desligamento recebido")
			turnoff.SetSystemState(SYSTEM_STATE_OFF)
			turnoff.Wait() //espera os wait groups terminarem

			//TODO: colocamos tudo que precisamos fazer antes do serviço morrer

			cachemx.Save()
			ControlLimit.Save()

			//salva os contadores
			CounterTR.Save()
			CounterBL.Save()
			//Fecha o arquivo de log
			Logger.Close()
			Logger.Printf(logger.INFO, "Pronto para morrer. Signal[%v]", s)
			os.Exit(0)
		}
	}
	return
}

//cria os diretorios dentro do data do projeto
func createDir() {
	os.MkdirAll(config.DataPath+CERT_DIR_FILES, 777)
	os.MkdirAll(config.DataPath+CACHEMX_DIR, 777)
	os.MkdirAll(config.DataPath+LIMIT_MX_DIR, 777)
	os.MkdirAll(config.DataPath+COUNTER_DIR, 777)
}

func startHttpServer() {

	// HTTP LISTEN

	http.HandleFunc("/sendmail", SendMailTRHttp)
	http.HandleFunc("/send-now", SendMailNWHttp)
	//http.HandleFunc("/send-test", SendTestHttp) //retirado url de envio de teste pois ela precisa ser corrigida. Ela havia sido criada apenas para testes iniciais

	http.HandleFunc("/simulate/send-bounce", SimulateSendBounceHttp)

	http.HandleFunc("/", ErrorHandler)
	if tr, ok := http.DefaultTransport.(*http.Transport); ok {
		tr.DisableKeepAlives = true
		tr.MaxIdleConnsPerHost = 1
		tr.CloseIdleConnections()
	}
	Logger.Printf(logger.INFO, "Iniciando serviço [%s] ...", config.HttpAddress)

	//serviço na porta definida
	server := &http.Server{Addr: config.HttpAddress, ReadTimeout: 2 * time.Second, WriteTimeout: 2 * time.Second}
	err := server.ListenAndServe()
	checkErrorAndKillMe(err)
}
