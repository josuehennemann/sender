package main

import (
	//"github.com/josuehennemann/logger"
	"sync"
	"time"
)

type controlRepeatSend struct {
	l    sync.Mutex
	list map[string]*itemControlRepeatSend
}

func NewControlRepeatSend() *controlRepeatSend {
	r := new(controlRepeatSend)
	r.list = map[string]*itemControlRepeatSend{}
	go r.expire()
	return r
}

type itemControlRepeatSend struct {
	content string
	date    time.Time
}

//valida se pode fazer o envio ou nao
func (this *controlRepeatSend) Block(e, html string) bool {
	this.l.Lock()
	defer this.l.Unlock()

	info, ok := this.list[e]
	//se ainda nao existe em memoria, se o conteudo é diferente ou faz mais de 24h, entao permite o envio
	if !ok || info.content != html || time.Since(info.date).Hours() > 24 {
		this.list[e] = &itemControlRepeatSend{content: html, date: time.Now()}
		return false
	}

	this.list[e] = &itemControlRepeatSend{content: html, date: time.Now()}
	return true
}

//rotina que verifica se tem alguem no mapa para expirar
func (this *controlRepeatSend) expire() {
	defer recoverPanic()
	for {
		// a cada 12h valida se tem para expirar
		time.Sleep(time.Hour * 12)
		this._expire()
	}
}

//funçao que realmente expira
func (this *controlRepeatSend) _expire() {
	this.l.Lock()
	defer this.l.Unlock()

	//deleta todo mundo que está há mais de 24h na memoria
	for k, v := range this.list {
		if time.Since(v.date).Hours() > 24 {
			delete(this.list, k)
		}
	}

}
