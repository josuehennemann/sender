package main

import (
	"encoding/json"
	"github.com/josuehennemann/logger"
	"io/ioutil"
	"strings"
	"sync"
	"time"
)

const (
	LIMIT_MX_DEFAULT = uint64(1000)
	LIMIT_MX_GOOGLE  = uint64(900)
	LIMIT_MX_YAHOO   = uint64(100) // reduzido de 300 para 100 para evitar bloqueio por limite no provedor
	LIMIT_MX_HOTMAIL = uint64(100) // reduzido de 300 para 100 para evitar bloqueio por limite no provedor
	LIMIT_MX_TERRA   = uint64(250)
	LIMIT_MX_UOL     = uint64(250)
	LIMIT_MX_DIR     = "limit_mx/"
)

type ControlLimitMX struct {
	list map[string]*ItemControlLimitMX // lista com os limites
	send map[string]*ItemControlLimitMX // lista com totais enviados por mx por ip
	file string                         //arquivo onde vai ser salvo a posição send
	sync.Mutex
}
type ItemControlLimitMX struct {
	total    uint64 //limite do mx
	lastSend time.Time
}

//inicializa o limitador de mx por minuto
func InitControlLimitMX() *ControlLimitMX {
	c := &ControlLimitMX{}
	c.list = map[string]*ItemControlLimitMX{
		"default":      &ItemControlLimitMX{total: LIMIT_MX_DEFAULT},
		"google.com":   &ItemControlLimitMX{total: LIMIT_MX_GOOGLE},
		"yahoodns.net": &ItemControlLimitMX{total: LIMIT_MX_YAHOO},
		"hotmail.com":  &ItemControlLimitMX{total: LIMIT_MX_HOTMAIL},
		"outlook.com":  &ItemControlLimitMX{total: LIMIT_MX_HOTMAIL},
		"terra.com":    &ItemControlLimitMX{total: LIMIT_MX_TERRA},
		"uol.com.br":   &ItemControlLimitMX{total: LIMIT_MX_UOL},
	}

	c.file = config.DataPath + LIMIT_MX_DIR + "data.json"
	c.send = map[string]*ItemControlLimitMX{}
	tmp, err := ioutil.ReadFile(c.file)

	txtLog := "Arquivo de limite de mx não encontrado, iniciando sem o contador"
	if len(tmp) > 0 && err == nil {
		txtLog = "Carregou arquivo de limite de mx"

		if err := json.Unmarshal(tmp, &c.send); err != nil {
			Logger.Printf(logger.ERROR, "Falha ao tentar fazer unmarshal do limite de mx. Erro[%s][%s]", err, string(tmp))
			return c
		}
	}

	Logger.Println(logger.INFO, txtLog)
	return c
}

//Valida se passou do limite
func (this *ControlLimitMX) CheckLimit(mx, ipOut string) bool {
	this.add(mx, ipOut) //incrementa no contador e depois verifica o limite
	return this.getTotalSend(mx, ipOut) >= this.getLimit(mx)
}

//salva em disco os totais enviados
func (this *ControlLimitMX) Save() {
	this.Lock()
	defer this.Unlock()
	data, err := json.Marshal(this.send)

	if err != nil {
		Logger.Printf(logger.ERROR, "Falha ao tentar fazer marshal do limite de mx. Erro[%s][%s]", err)
		return
	}

	err = ioutil.WriteFile(this.file, data, 0666)
	if err != nil {
		Logger.Printf(logger.ERROR, "Falha ao tentar salvar do limite de mx. Erro[%s][%s]", err, string(data))
		return
	}

	Logger.Printf(logger.INFO, "Arquivo de limite de mx salvo com sucesso")
}

//descobre o limite por minuto de um mx
func (this *ControlLimitMX) getLimit(mx string) uint64 {
	total := this.list["default"].total
	for k, v := range this.list {
		if strings.Contains(mx, k) {
			total = v.total
			break
		}
	}
	return total
}

//Incrementa um no contator desse mx nesse ip de saida
func (this *ControlLimitMX) add(mx, ipOut string) {
	this.Lock()
	defer this.Unlock()
	key := this.buildKey(mx, ipOut)
	if _, ok := this.send[key]; !ok {
		this.send[key] = &ItemControlLimitMX{lastSend: time.Now()}
	}
	//se faz mais de 1 minuto desde o ultimo envio então zera o contador
	if time.Since(this.send[key].lastSend) > time.Minute {
		this.send[key].lastSend = time.Now()
		this.send[key].total = uint64(0)
	}

	this.send[key].total++
}

//Retorna o total de envios já realizado para o mx por aquele IP
func (this *ControlLimitMX) getTotalSend(mx, ipOut string) (t uint64) {
	this.Lock()
	defer this.Unlock()
	if v, ok := this.send[this.buildKey(mx, ipOut)]; ok {
		t = v.total
	}
	return t
}

//monta a chave do mapa de totais enviados
func (this *ControlLimitMX) buildKey(mx, ipOut string) string {
	return mx + "-" + ipOut
}
