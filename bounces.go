/************TABELA DE ERROS ***********
* CC - Caixa cheia                    *
* FE - Falha de entrega               *
* FI - Falha interna                  *
* CI - Conta indisponivel ou invalida *
* SI - Servidor indisponivel          *
* DI - Dominio invalido               *
* NA - Não autorizado (Groupos)       *
* AS - Bloqueio Anti-SPAM             *
* ED - Erro desconhecido              *
***************************************/

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/josuehennemann/logger"
	"net/http"
	"regexp"
	"strings"
	"time"
)

type BounceCode string

func (this BounceCode) String() string {
	describe := "[n/a]"
	switch this {
	case BOUNCE_TYPE_MF:
		describe = "Mailbox full"
	case BOUNCE_TYPE_DF:
		describe = "Delivery failure"
	case BOUNCE_TYPE_IE:
		describe = "Internal error"
	case BOUNCE_TYPE_AU:
		describe = "Account unavailable"
	case BOUNCE_TYPE_SU:
		describe = "Server unavailable"
	case BOUNCE_TYPE_ID:
		describe = "Invalid domain"
	case BOUNCE_TYPE_NA:
		describe = "Not authorized (Groups)"
	case BOUNCE_TYPE_AS:
		describe = "Anti-SPAM lock"
	case BOUNCE_TYPE_EU:
		describe = "Error unknown"
	}
	return string(this) + " (" + describe + ")"
}

const (
	BOUNCE_TYPE_MF = BounceCode("MF")
	BOUNCE_TYPE_DF = BounceCode("DF")
	BOUNCE_TYPE_IE = BounceCode("IE")
	BOUNCE_TYPE_AU = BounceCode("AU")
	BOUNCE_TYPE_SU = BounceCode("SU")
	BOUNCE_TYPE_ID = BounceCode("ID")
	BOUNCE_TYPE_NA = BounceCode("NA")
	BOUNCE_TYPE_AS = BounceCode("AS")
	BOUNCE_TYPE_EU = BounceCode("EU")

	MAX_RETRY_SEND_BOUNCE = 5
)

type JsonSendBounce struct {
	Code        string `json:"code"`
	Description string `json:"description"`
	CodeSend    string `json:"code_send"`
}

type BounceData struct {
	CodeSend     string // codigo do envio
	Bounce       BounceCode
	BounceText   string
	SendBounceTo *InfoBounce

	//campos não utilizados no momento
	Code       string //codigo do contato, utilizado para saber tratar bounce, no messageId e log
	Email      string // email do contato
	ClientCode string //Codigo do cliente,  utilizado para saber tratar bounce, no messageId e log

}

var (
	regexpGetNumbers      = regexp.MustCompile("^([0-9]*) ")
	regexpBounceCodeFloat = regexp.MustCompile("[45]\\.([0-9]\\.[0-9])")
	regexpAntiRBL         = regexp.MustCompile("rbl[^a-z0-9]")
	regexpUserUnknow      = regexp.MustCompile("user <[^>]*> unknown")
	errorMaxRetry         = errors.New("605 - Max send rety")
)

func (bd *BounceData) String() string {
	return fmt.Sprintf(" Envio [%s] Bounce [%s-%s]", bd.CodeSend, bd.Bounce, bd.BounceText)
}

//Metodo que envia o bounce para o sistema de origem
//retorna um boleano indicando se conseguiu ou não fazer a requisição
func (bd *BounceData) SendRequest() bool {
	// se nao tem para onde enviar entao sai da função
	if bd.SendBounceTo == nil || bd.SendBounceTo.Url == ""{
		return true
	}

	infoPost := &JsonSendBounce{
		Code:        string(bd.Bounce),
		Description: bd.Bounce.String(),
		CodeSend:    bd.CodeSend,
	}

	dataBody, err := json.Marshal(infoPost)
	if err != nil {
		Logger.Printf(logger.ERROR, "Falha ao fazer marshal de bounce. infoPost [%+v] Erro [%s] %s", infoPost, err, bd)
		return true
	}

	client := &http.Client{
		Transport: http.DefaultTransport,
		Timeout:   time.Second * 30,
	}
	retry := 0
	for {
		retry++
		//caso o sistema tenha entrado em desligamento, então desiste de fazer a requisição e retorna false para nao dar pop na fila
		if turnoff.IsShutdown() {
			Logger.Printf(logger.INFO, "Sistema em desligamento, parando de tentar enviar bounce. Url [%s] %s", bd.SendBounceTo.Url, bd)
			return false
		}
		//valida se chegou no limite de tentativas
		if retry >= MAX_RETRY_SEND_BOUNCE {
			return true
		}

		//monta o body da requisição
		body := &bytes.Buffer{}
		body.Write(dataBody)
		//monta a requisição
		req, err := http.NewRequest(http.MethodPost, bd.SendBounceTo.Url, body)
		if err != nil {
			Logger.Printf(logger.ERROR, "Falha ao montar requisição de bounce. Url [%s] Erro [%s] %s", bd.SendBounceTo.Url, err, bd)
			continue
		}
		defer req.Body.Close()
		//define o token de Authorization, para enviar o bounce
		req.Header.Set("Authorization", config.Authorization)

		//Faz a requisição em si
		resp, err := client.Do(req)
		if err != nil {
			Logger.Printf(logger.ERROR, "Falha ao fazer requisição de bounce. Url [%s] Erro [%s] %s", bd.SendBounceTo.Url, err, bd)
			continue
		}

		//valida se deu 200
		// qualquer coisa diferente de 200 trata como erro
		if resp.StatusCode != http.StatusOK {
			Logger.Printf(logger.ERROR, "Falha ao fazer requisição de bounce. Url [%s] Retorno [%d] %s", bd.SendBounceTo.Url, resp.StatusCode, bd)
			continue
		}
		// se chegou aqui então funcionou o post
		break
	}
	return true
}

//classifica o erro do envio
func (bd *BounceData) bounceClassifier() {
	bd.Bounce = parseAndBounceClassifier(bd.BounceText)
	//Caso o bounce tenha sido classificado como erro desconhecido
	// entao gravamos um log um dia ajudar a melhorar a classificação
	if bd.Bounce == BOUNCE_TYPE_EU {
		Logger.Printf(logger.INFO, "Erro [%s] classificado como desconhecido. E-mail [%s]", bd.BounceText, bd.Email)
	}
}

//função que realmente classifica o erro, feita separada para poder implementar teste unitario
//define como padrao o erro desconhecido, e depois tenta classificar em outro tipo
func parseAndBounceClassifier(bounceText string) BounceCode {
	bounceText = strings.ToLower(bounceText)
	bounce := BOUNCE_TYPE_EU

	matches := regexpGetNumbers.FindStringSubmatch(bounceText)
	//valida que conseguiu pegar o codigo da mensagem de erro do provedor
	if len(matches) > 1 && len(matches[1]) == 3 {
		codErr := matches[1]

		// Erros padroes para todos os commandos
		switch codErr {
		case // Servico indisponivel
			"451", // Requested action aborted: local error in processing
			"500", // Syntax error, command unrecognised
			"501", // Syntax error in parameters or arguments
			"503", // Bad sequence of commands
			"504", // Command parameter not implemented
			"521": // <domain> does not accept mail [rfc1846]
			bounce = BOUNCE_TYPE_SU
		case // Caixa postal cheia
			"452", // Requested action not taken: insufficient system storage
			"552": // Requested mail action aborted: exceeded storage allocation
			bounce = BOUNCE_TYPE_MF
		case // E-mail invalido
			"251", // User not local; will forward to <forward-path>
			"551", // User not local; please try <forward-path>
			"252": //The server cannot verify the user
			bounce = BOUNCE_TYPE_AU
			if strings.Contains(bounceText, "forbidden by your ip") || strings.Contains(bounceText, "entrega recusada") {
				bounce = BOUNCE_TYPE_AS
			}
		case // Falha de entrega
			"421", // <domain> Service not available, closing transmission channel,  421 hotmail e yahoo utilizam esse codigo para sinalizar bloqueio
			"450",
			"554",
			"550":
			bounce = BOUNCE_TYPE_DF
		case // Dominio Invalido
			"601", // Dominio nao encontrado
			"602", // Dominio sem MX
			"653":
			bounce = BOUNCE_TYPE_ID
		case //servidor indisponivel
			"603", // MX nao responde
			"604": // Timeout
			bounce = BOUNCE_TYPE_SU
		case //Falha interna
			"605":
			bounce = BOUNCE_TYPE_IE
		case // bloqueio antispam
			"610":
			bounce = BOUNCE_TYPE_AS
		case // nao autorizado, grupos
			"620":
			bounce = BOUNCE_TYPE_NA
		default:
			if regexpUserUnknow.MatchString(bounceText) {
				bounce = BOUNCE_TYPE_AU
			}
		}
		//se chegou aqui e nao é desconhecido entao sai fora
		if bounce != BOUNCE_TYPE_EU {
			return bounce
		}
	}

	// Se tem status ja paraliza com o erro encontrado

	matches = regexpBounceCodeFloat.FindStringSubmatch(bounceText)
	if len(matches) > 1 {
		delivery_code := matches[1]
		switch delivery_code {
		case
			"1.1", // User unknown
			"2.1": // Mailbox disabled
			bounce = BOUNCE_TYPE_AU
			if strings.Contains(bounceText, "cloudmark sender intelligence") {
				bounce = BOUNCE_TYPE_AS
			}
		case
			"2.2": // Mailbox full
			bounce = BOUNCE_TYPE_MF
		case
			"4.7": // Time expired
			bounce = BOUNCE_TYPE_DF
		}
		return bounce
	}

	// Procura nas strings padrões se encontra um texto conhecido
	for _, v := range codestStdSmtpErrors {
		if strings.Contains(bounceText, v.text) {
			bounce = v.code
			break
		}
	}
	return bounce
}

type itemBounceClassifier struct {
	text string
	code BounceCode
}

var codestStdSmtpErrors = []*itemBounceClassifier{
	&itemBounceClassifier{"other undefined", BOUNCE_TYPE_EU},
	&itemBounceClassifier{"other address status", BOUNCE_TYPE_EU},
	&itemBounceClassifier{"bad destination mailbox address", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"bad destination system address", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"bad destination mailbox address syntax", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"destination mailbox address ambiguous", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"destination address valid", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"destination mailbox has moved, no forwarding address", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"bad sender's mailbox address syntax", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"bad sender's system address", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"bad email address", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"incorrect email address", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"message relayed to non-compliant mailer", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"recipient address has null mx", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"other or undefined mailbox status", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"mailbox disabled, not accepting messages", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"mailbox full", BOUNCE_TYPE_MF},
	&itemBounceClassifier{"no such mailbox", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"message length exceeds administrative limit", BOUNCE_TYPE_MF},
	&itemBounceClassifier{"mailing list expansion problem", BOUNCE_TYPE_MF},
	&itemBounceClassifier{"other or undefined mail system status", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"mail system full", BOUNCE_TYPE_MF},
	&itemBounceClassifier{"system not accepting network messages", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"system not capable of selected features", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"message too big for system", BOUNCE_TYPE_MF},
	&itemBounceClassifier{"system incorrectly configured", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"requested priority was changed", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"other or undefined network or routing status", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"no answer from host", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"bad connection", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"directory server failure", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"unable to route", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"mail system congestion", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"routing loop detected", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"delivery time expired", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"other or undefined protocol status", BOUNCE_TYPE_EU},
	&itemBounceClassifier{"invalid command", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"syntax error", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"too many recipients", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"invalid command arguments", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"wrong protocol version", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"authentication exchange line is too long", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"other or undefined media error", BOUNCE_TYPE_EU},
	&itemBounceClassifier{"media not supported", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"conversion required and prohibited", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"conversion required but not supported", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"conversion with loss performed", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"conversion failed", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"message content not available", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"non-ascii addresses not permitted for that sender/recipient", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"utf-8 string reply is required,but not permitted by the smtp client", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"utf-8 header message cannot be transferred to one or more recipients, so the message must be rejected", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"other or undefined security status", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"delivery not authorized, message refused", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"mailing list expansion prohibited", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"security conversion required but not possible", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"security features not supported", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"cryptographic failure", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"cryptographic algorithm not supported", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"message integrity failure", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"authentication credentials invalid", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"authentication mechanism is too weak", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"encryption needed", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"encryption required for requested authentication mechanism", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"a password transition is needed", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"user account disabled", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"refused: user invalid", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"... user not known", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"trust relationship required", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"priority level is too low", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"message is too big for the specified priority", BOUNCE_TYPE_MF},
	&itemBounceClassifier{"mailbox owner has changed", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"domain owner has changed", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"rrvs test cannot be completed", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"no passing dkim signature found", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"no acceptable dkim signature found", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"no valid author-matched dkim signature found", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"spf validation failed", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"spf validation error", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"reverse dns validation failed", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"multiple authentication checks failed", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"sender address has null mx", BOUNCE_TYPE_DF},
	&itemBounceClassifier{"spam", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"group", BOUNCE_TYPE_NA},
	&itemBounceClassifier{"nf", BOUNCE_TYPE_EU},
	&itemBounceClassifier{"part of their network is on our block list", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"spam message rejected", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"bad destination mailbox address", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"delivery not authorized, message refused", BOUNCE_TYPE_NA},
	&itemBounceClassifier{"restricted domain", BOUNCE_TYPE_AS},       // UOL Anti-spam
	&itemBounceClassifier{"no mailbox by that name", BOUNCE_TYPE_AU}, // Caixa Invalida
	&itemBounceClassifier{"sem resolucao de mx", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"user doesn't have a yahoo", BOUNCE_TYPE_AU}, //554 para o yahoo
	&itemBounceClassifier{"unknown user", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"no mailbox here by that name", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"usuario desconhecido", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"account has been disabled", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"user unknown", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"host unknown", BOUNCE_TYPE_ID},
	&itemBounceClassifier{"mailbox unavailable", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"no such user", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"we have limits for how many messages", BOUNCE_TYPE_AS}, // Mensagem de bloqueio hotmail
	&itemBounceClassifier{"4.16.55", BOUNCE_TYPE_AS},                              // Mensagem de bloqueio yahoo
	&itemBounceClassifier{"no such recipient", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"temporarily unavailable", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"recipient not known", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"to dnsbl", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"blocked", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"service unavailable", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"no such host", BOUNCE_TYPE_AU}, // Mensagem do Go para mx que nao resolve
	&itemBounceClassifier{"mailbox quota exceeded", BOUNCE_TYPE_MF},
	&itemBounceClassifier{"click21.com.br", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"address rejected", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"poor network reputation", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"denied by policy", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"connection timed out", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"connection refused", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"too many invalid recipients", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"no such host", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"não existe", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"invalid recipient", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"unrouteable address", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"unusual rate", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"like spam", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"no mailbox", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"unsolicited", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"recipient unknown", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"as spam", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"email bloqueado", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"user doesn't have", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"nao esta autorizado", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"i/o timeout", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"connection timed out", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"getsockopt: ", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"read: connection ", BOUNCE_TYPE_SU},
	&itemBounceClassifier{"no such domain at this location", BOUNCE_TYPE_ID},
	&itemBounceClassifier{"domain does not exist", BOUNCE_TYPE_ID},
	&itemBounceClassifier{"nenhuma pessoa no endere", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"no such person here", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"550 user not found", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"550 address unknown", BOUNCE_TYPE_AU},
	&itemBounceClassifier{"554 5.7.1", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"454 4.7.1", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"csi poor", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"sender rejected", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"has been denied", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"in my badmailfrom list", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"rejecting banned content", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"550 #5.7.1", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"intrusion prevention active", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"junkmail rejected", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"message not allowed", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"blacklist", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"policy violation", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"security policies", BOUNCE_TYPE_AS},
	&itemBounceClassifier{"mailbox has exceeded its storage limit.", BOUNCE_TYPE_MF},
	&itemBounceClassifier{"space on the disk", BOUNCE_TYPE_MF},
	&itemBounceClassifier{"server is not responding", BOUNCE_TYPE_SU},
}
